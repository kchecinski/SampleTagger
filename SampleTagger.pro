#-------------------------------------------------
#
# Project created by QtCreator 2018-03-15T13:13:40
#
#-------------------------------------------------

QT       += core gui \
            multimedia \
            multimediawidgets

CONFIG  += console

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SampleTagger
TEMPLATE = app


SOURCES += main.cpp \
    MainWindow.cpp \
    SamplePlayer.cpp \
    PlaylistModel.cpp \
    ControlPanel.cpp \
    SampleSlider.cpp \
    SliderFrame.cpp \
    Tagger.cpp \
    KeywordsBase.cpp \
    TextResourceBase.cpp \
    Sample.cpp \
    TaggedSamplesBase.cpp \
    KeywordsBrowser.cpp \
    SampleProcess.cpp \
    ProcessingWindow.cpp \
    SampleEditor.cpp \
    LabelInput.cpp

HEADERS  += \
    MainWindow.h \
    SamplePlayer.h \
    PlaylistModel.h \
    ControlPanel.h \
    SampleSlider.h \
    SliderFrame.h \
    Tagger.h \
    KeywordsBase.h \
    TextResourceBase.h \
    Sample.h \
    TaggedSamplesBase.h \
    KeywordsBrowser.h \
    SampleProcess.h \
    ProcessingWindow.h \
    SampleEditor.h \
    LabelInput.h

FORMS    += mainwindow.ui \
    SamplePlayer.ui \
    ControlPanel.ui \
    Tagger.ui \
    KeywordsBrowser.ui \
    ProcessingWindow.ui \
    SampleEditor.ui
