#include "ControlPanel.h"
#include "ui_ControlPanel.h"

#include <QPushButton>
#include <QSlider>
#include <QShortcut>
#include <QSpinBox>

ControlPanel::ControlPanel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ControlPanel)
{
    ui->setupUi(this);

    connect(ui->playButton, &QPushButton::clicked, this, &ControlPanel::playClicked);
    connect(ui->nextButton, &QPushButton::clicked, this, &ControlPanel::next);
    connect(ui->previousButton, &QPushButton::clicked, this, &ControlPanel::previous);
    connect(ui->windBackButton, &QPushButton::clicked, this, &ControlPanel::windBack);
    connect(ui->windForwardButton, &QPushButton::clicked, this, &ControlPanel::windForward);
    connect(ui->alignFrameButton, &QPushButton::clicked, this, &ControlPanel::alignFrame);
    connect(ui->playFrameButton, &QPushButton::clicked, this, &ControlPanel::playFrame);
    connect(ui->clipFrameButton, &QPushButton::clicked, this, &ControlPanel::clipFrame);
    connect(ui->volumeSlider, &QSlider::valueChanged, this, &ControlPanel::volumeChange);
    connect<void(QSpinBox::*)(int)>(ui->frameLengthSpinBox, &QSpinBox::valueChanged, this, &ControlPanel::frameLengthChanged);
    connect(ui->manualSlider, &QSlider::valueChanged, this, &ControlPanel::handleManualSliderMove);

    state_ = QMediaPlayer::StoppedState;

    assignShortcuts();
}

ControlPanel::~ControlPanel()
{
    delete ui;
}

int ControlPanel::volume() const
{
    return ui->volumeSlider->value();
}

int ControlPanel::frameLength() const
{
    return ui->frameLengthSpinBox->value();
}

void ControlPanel::assignShortcuts()
{
    static QShortcut playShortcut(QKeySequence("Space"), this);
    static QShortcut windBackShortcut(QKeySequence("Left"), this);
    static QShortcut windForwardShortcut(QKeySequence("Right"), this);
    static QShortcut playFrameShortcut(QKeySequence("Shift+Space"), this);
    static QShortcut clipFrameShortcut(QKeySequence("Shift+C"), this);

    connect(&playShortcut, &QShortcut::activated, ui->playButton, &QAbstractButton::click);
    connect(&windBackShortcut, &QShortcut::activated, ui->windBackButton, &QAbstractButton::click);
    connect(&windForwardShortcut, &QShortcut::activated, ui->windForwardButton, &QAbstractButton::click);
    connect(&playFrameShortcut, &QShortcut::activated, ui->playFrameButton, &QAbstractButton::click);
    connect(&clipFrameShortcut, &QShortcut::activated, ui->clipFrameButton, &QAbstractButton::click);
}



void ControlPanel::setState(QMediaPlayer::State state)
{
    this->state_ = state;
    switch (state) {
    case QMediaPlayer::StoppedState:
        ui->playButton->setText(QString("\u25B6"));
        emit pause();
        break;
    case QMediaPlayer::PlayingState:
        ui->playButton->setText(QString("\u23F8"));
        emit play();
        break;
    case QMediaPlayer::PausedState:
        ui->playButton->setText(QString("\u25B6"));
        emit pause();
        break;

    }
}

void ControlPanel::setSampleLength(int length)
{
    ui->manualSlider->setMaximum(length);
}

void ControlPanel::updateManualSlider(int position)
{
    if (!(ui->manualSlider->isSliderDown()))
        ui->manualSlider->setValue(position);
}

void ControlPanel::samplePlayed()
{
    setState(QMediaPlayer::PlayingState);
}




void ControlPanel::playClicked()
{
    switch (state_) {
    case QMediaPlayer::StoppedState:
        setState(QMediaPlayer::PlayingState);
        break;
    case QMediaPlayer::PlayingState:
        setState(QMediaPlayer::PausedState);
        break;
    case QMediaPlayer::PausedState:
        setState(QMediaPlayer::PlayingState);
        break;
    }
}

void ControlPanel::handleManualSliderMove(int position)
{
    if (ui->manualSlider->isSliderDown())
    {
        emit manualSliderMoved(position);
    }
}
