#include "KeywordsBase.h"

#include <QFile>
#include <QMessageBox>
#include <QStringList>
#include <QMap>
#include <QDebug>

KeywordsBase::KeywordsBase()
{

}

void KeywordsBase::parseFile(QFile &file)
{
    QStringList keywords = QString(file.readAll()).split('\n');
    keywords.removeAt(keywords.size() - 1);
    setStringList(keywords);
}

void KeywordsBase::saveFile(QFile &file)
{
    QStringList keywords = getStringList();
    QTextStream s(&file);
    for (auto it = keywords.begin(); it != keywords.end(); ++it) {
        s << *it << '\n';
    }
}

QString KeywordsBase::fileFormat() const
{
    static QString format("Keywords base files (*.kwb)");
    return format;
}
