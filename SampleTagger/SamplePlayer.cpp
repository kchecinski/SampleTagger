#include "SamplePlayer.h"
#include "ui_SamplePlayer.h"

#include <QMessageBox>

SamplePlayer::SamplePlayer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SamplePlayer)
{
    ui->setupUi(this);
    ui->playlistView->setModel(&playlist_model_);

    playlist_model_.setPlaylist(&playlist_);
    player_.setPlaylist(&playlist_);
    player_.setNotifyInterval(20);

    connect(ui->controlPanel, &ControlPanel::play, &player_, &QMediaPlayer::play);
    connect(ui->controlPanel, &ControlPanel::pause, &player_, &QMediaPlayer::pause);
    connect(ui->controlPanel, &ControlPanel::next, &playlist_, &QMediaPlaylist::next);
    connect(ui->controlPanel, &ControlPanel::previous, &playlist_, &QMediaPlaylist::previous);
    connect(ui->controlPanel, &ControlPanel::windBack, this, &SamplePlayer::windBack);
    connect(ui->controlPanel, &ControlPanel::windForward, this, &SamplePlayer::windForward);
    connect(ui->controlPanel, &ControlPanel::manualSliderMoved, &player_, &QMediaPlayer::setPosition);
    connect(ui->controlPanel, &ControlPanel::volumeChange, &player_, &QMediaPlayer::setVolume);
    connect(ui->controlPanel, &ControlPanel::clipFrame, this, &SamplePlayer::exactSnatch);
    connect(ui->controlPanel, &ControlPanel::frameLengthChanged, ui->slider, &SampleSlider::setFrameLength);
    connect(ui->controlPanel, &ControlPanel::playFrame, this, &SamplePlayer::playFrame);

    connect(ui->playlistView, &QListView::doubleClicked, this, &SamplePlayer::sampleDoubleClicked);
    connect(ui->playlistView, &QListView::doubleClicked, ui->controlPanel, &ControlPanel::samplePlayed);
    connect(&playlist_, &QMediaPlaylist::currentIndexChanged, this, &SamplePlayer::activeSampleChanged);

    connect(&player_, &QMediaPlayer::stateChanged, ui->controlPanel, &ControlPanel::setState);
    connect(&player_, &QMediaPlayer::durationChanged, ui->slider, &SampleSlider::setSampleLength);
    connect(&player_, &QMediaPlayer::positionChanged, ui->slider, &SampleSlider::setPosition);
    connect(&player_, &QMediaPlayer::stateChanged, ui->slider, &SampleSlider::setState);
    connect(&player_, &QMediaPlayer::durationChanged, ui->controlPanel, &ControlPanel::setSampleLength);
    connect(&player_, &QMediaPlayer::positionChanged, ui->controlPanel, &ControlPanel::updateManualSlider);

    connect(ui->slider, &SampleSlider::endOfFrameReached, this, &SamplePlayer::alignPlayerToFrame);
    connect(ui->slider, &SampleSlider::endOfFrameReached, &player_, &QMediaPlayer::pause);

    ui->slider->setFrameLength(ui->controlPanel->frameLength());

}

SamplePlayer::~SamplePlayer()
{
    delete ui;
}

void SamplePlayer::addToPlaylist(const QList<QUrl> urls)
{
    foreach (const QUrl &url, urls) {
        playlist_.addMedia(url);
        loaded_paths_.append(url.path());
    }
}

void SamplePlayer::addToPlaylist(const QUrl url)
{
    playlist_.addMedia(url);
    loaded_paths_.append(url.toString());
}

void SamplePlayer::on_OpenFilesButton_clicked()
{
    QList<QUrl> urls = QFileDialog::getOpenFileUrls(this, "Open samples", QUrl(""), "WAV, MP3 files (*.wav *.mp3)");
    addToPlaylist(urls);
}

void SamplePlayer::sampleDoubleClicked(const QModelIndex &index)
{
    playlist_.setCurrentIndex(index.row());
    player_.play();
}

void SamplePlayer::activeSampleChanged(int index)
{
    ui->playlistView->setCurrentIndex(playlist_model_.index(index, 0));
}

void SamplePlayer::playSample(const Sample &sample)
{
    int index = loaded_paths_.indexOf(sample.getPath());
    if (index == -1) {
        QMessageBox msg;
        msg.setText("File with choosen sample have not been loaded. Load this file first.");
        msg.exec();
        return;
    }
    int begin = sample.getBegin();
    playlist_.setCurrentIndex(index);
    player_.setPosition(begin);
    ui->controlPanel->setState(QMediaPlayer::State::PlayingState);
}

void SamplePlayer::setFocusOn()
{
    setFocus();
}

void SamplePlayer::windBack()
{
    player_.setPosition(player_.position()-WIND_SIZE);
}

void SamplePlayer::windForward()
{
    player_.setPosition(player_.position()+WIND_SIZE);
}

void SamplePlayer::playFrame()
{
    long position = ui->slider->getFrameBeginPosition();
    if (position < 0)
        position = 0;
    player_.setPosition(position);
    ui->slider->setMode(SliderFrame::Mode::Bar);
    player_.play();
}

void SamplePlayer::stopFramePlaying()
{
    player_.pause();
}

void SamplePlayer::alignPlayerToFrame()
{
    int position = ui->slider->getFrame().getPosition();
    player_.setPosition(position);
}

void SamplePlayer::exactSnatch()
{
    auto frame = ui->slider->getFrame();
    int begin = frame.getFrameBeginPosition();
    int end = frame.getPosition();
    QUrl url = player_.currentMedia().canonicalUrl();
    emit snatchGenerated(Sample(url, begin, end));
}
