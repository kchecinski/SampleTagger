#include "SampleEditor.h"
#include "ui_SampleEditor.h"

#include <QShortcut>
#include <QSpinBox>

SampleEditor::SampleEditor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SampleEditor)
{
    ui->setupUi(this);
    connect(ui->ok_btn, &QPushButton::clicked, this, &SampleEditor::okButtonClicked);
//    connect(ui->delete_btn, &QPushButton::clicked, this, &SampleEditor::deleteButtonClicked);

    connect(ui->labels_input, &LabelInput::keywordConfirmed, this, &SampleEditor::addLabel);

    static QShortcut okShortcut(QKeySequence("Enter"), this);
    static QShortcut deleteShortcut(QKeySequence("Delete"), this);

    connect(&okShortcut, &QShortcut::activated, ui->ok_btn, &QAbstractButton::click);
//    connect(&deleteShortcut, &QShortcut::activated, ui->delete_btn, &QAbstractButton::click);

    connect(ui->start_input, SIGNAL(valueChanged(int)), this, SLOT(synchronizeEnd(int)));
    connect(ui->end_input, SIGNAL(valueChanged(int)), this, SLOT(synchronizeBegin(int)));
}

SampleEditor::~SampleEditor()
{
    delete ui;
}

QSet<int> SampleEditor::getLabels() const
{
    QSet<int> labels;
    for (auto label: ui->labels_ids->text().split(',')) {
        labels.insert(label.toInt());
    }
    return labels;
}

int SampleEditor::getStart() const
{
    return ui->start_input->value();
}

int SampleEditor::getEnd() const
{
    return ui->end_input->value();
}

QString SampleEditor::getAdditionalInfo() const
{
    return ui->add_info_input->text();
}

QModelIndex SampleEditor::getIndex() const
{
    return index_;
}

void SampleEditor::addLabel(int id)
{
    labels_str_ += "," + QString::number(id);
    ui->labels_ids->setText(labels_str_);
}

void SampleEditor::synchronizeBegin(int i)
{
    ui->start_input->setValue(i - frame_length_);
}

void SampleEditor::synchronizeEnd(int i)
{
    ui->end_input->setValue(i + frame_length_);
}

void SampleEditor::setSample(const Sample& sample, QModelIndex index)
{
    index_ = index;
    QStringList labels;
    for (int label: sample.getIds()) {
        labels.append(QString::number(label));
    }
    labels_str_ = labels.join(",");
    frame_length_ = sample.getEnd() - sample.getBegin();
    ui->labels_ids->setText(labels_str_);
    ui->start_input->setValue(sample.getBegin());
    ui->end_input->setValue(sample.getEnd());
    ui->add_info_input->setText(sample.getAdditionalInfo());
}

void SampleEditor::setKeywordsBase(KeywordsBase* base)
{
    ui->labels_input->setKeywordsBase(base);
}
