#ifndef KEYWORDSBASE_H
#define KEYWORDSBASE_H

#include <QStringListModel>
#include <QUrl>

#include "TextResourceBase.h"

class KeywordsBase : public TextResourceBase
{
    Q_OBJECT

public:
    KeywordsBase();
    KeywordsBase(const QString& path);

public slots:

signals:

private:
    void parseFile(QFile& file) override;
    void saveFile(QFile& file) override;

    QString fileFormat() const override;

    QMap<QString, int> map_;
};

#endif // KEYWORDSBASE_H
