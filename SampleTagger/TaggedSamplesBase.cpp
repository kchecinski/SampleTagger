#include "TaggedSamplesBase.h"

#include <QDebug>

const QString TaggedSamplesBase::separator = "\t";
const QString TaggedSamplesBase::ids_separator = ",";

TaggedSamplesBase::TaggedSamplesBase()
{}

void TaggedSamplesBase::add(const Sample &sample)
{
    samples_.append(sample);
    QString info = sample.toString(separator, ids_separator);
    addString(info);
}

void TaggedSamplesBase::parseFile(QFile &file)
{
    QStringList samples = QString(file.readAll()).split('\n');
    samples.removeAt(samples.size() - 1);
    setStringList(samples);
}

void TaggedSamplesBase::saveFile(QFile &file)
{
    QStringList samples = getStringList();
    QTextStream s(&file);
    for (auto it = samples.begin(); it != samples.end(); ++it) {
        s << *it << '\n';
    }
}

