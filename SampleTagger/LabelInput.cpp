#include "LabelInput.h"

LabelInput::LabelInput(QWidget *parent):
    QLineEdit(parent)
{
    setCompleter(&completer_);
    completer_.setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
    connect(this, &QLineEdit::returnPressed, this, &LabelInput::checkInput);
}

LabelInput::LabelInput(const QString &contents, QWidget *parent):
    QLineEdit(contents, parent)
{
    setCompleter(&completer_);
    completer_.setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
    connect(this, &QLineEdit::returnPressed, this, &LabelInput::checkInput);
}

LabelInput::~LabelInput()
{}

void LabelInput::setKeywordsBase(KeywordsBase *keywords)
{
    keywords_ = keywords;
    keywords_->applyModel(&completer_);
}

void LabelInput::checkInput()
{
    if (isReadOnly())
        return;
    QString keyword = text();
    if (!keyword.isEmpty()) {
        int id = keywords_->find(keyword);
        if (id == -1 || keyword.isEmpty())
            emit undefinedKeywordUsed(keyword);
        else
            emit keywordConfirmed(id);
        clear();
    }
    else
        emit emptyInputConfirmed();
}


