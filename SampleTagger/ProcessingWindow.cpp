#include "ProcessingWindow.h"
#include "ui_ProcessingWindow.h"

#include <QDebug>
#include <QMap>

ProcessingWindow::ProcessingWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProcessingWindow)
{
    ui->setupUi(this);
    connect(ui->ok_btn, &QAbstractButton::clicked, this, &ProcessingWindow::close);
}

ProcessingWindow::~ProcessingWindow()
{
    delete ui;
}

int ProcessingWindow::getSamplesCount() const
{
    return samples_count_;
}

void ProcessingWindow::connectProcess(const SampleProcess *process)
{
    if (!process)
        return;

    process_ = process;
    connect(process_, &SampleProcess::sampleProcessed,
            this, &ProcessingWindow::setProcessedSamples);
    connect(process_, &SampleProcess::startStage,
            this, &ProcessingWindow::startStage);
    connect(process_, &SampleProcess::processingFinished,
            this, &ProcessingWindow::finishProcessing);
    connect(process_, &SampleProcess::setElementsCount,
            this, &ProcessingWindow::setElementsCount);
    connect(ui->abort_btn, &QAbstractButton::clicked, process_, &SampleProcess::abort);
    connect(ui->abort_btn, &QAbstractButton::clicked, this, &ProcessingWindow::abortProcess);
}

void ProcessingWindow::disconnectProcess()
{
    if (!process_)
        return;

    disconnect(process_, &SampleProcess::sampleProcessed,
               this, &ProcessingWindow::setProcessedSamples);
    disconnect(process_, &SampleProcess::startStage,
            this, &ProcessingWindow::startStage);
    disconnect(process_, &SampleProcess::processingFinished,
            this, &ProcessingWindow::finishProcessing);
    disconnect(process_, &SampleProcess::setElementsCount,
            this, &ProcessingWindow::setElementsCount);
    disconnect(ui->abort_btn, &QAbstractButton::clicked, process_, &SampleProcess::abort);
    connect(ui->abort_btn, &QAbstractButton::clicked, this, &ProcessingWindow::abortProcess);
    delete process_;
    process_ = nullptr;

}

void ProcessingWindow::setProcessedSamples(int count)
{
    ui->progress_bar->setValue(count);
}

void ProcessingWindow::setElementsCount(int count)
{
    samples_count_ = count;
    ui->progress_bar->setMaximum(count);
}

void ProcessingWindow::startStage(const QString &description)
{
    ui->label->setText(description);
    ui->progress_bar->setValue(0);
}

void ProcessingWindow::finishProcessing()
{
    ui->label->setText("Processing finished");
    ui->ok_btn->setEnabled(true);
    ui->abort_btn->setEnabled(false);
}

void ProcessingWindow::abortProcess()
{
    ui->label->setText("Processing aborted");
    ui->abort_btn->setEnabled(false);
    ui->ok_btn->setEnabled(true);

}
