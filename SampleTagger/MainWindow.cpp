#include "MainWindow.h"
#include "ui_mainwindow.h"
#include <iostream>

#include "SamplePlayer.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->sample_player, &SamplePlayer::snatchGenerated, this, &MainWindow::printSnatch);
    connect(ui->sample_player, &SamplePlayer::snatchGenerated, ui->tagger, &Tagger::setCurrentSnatch);
    connect(ui->tagger, &Tagger::sampleChosen, ui->sample_player, &SamplePlayer::playSample);
    connect(ui->tagger, &Tagger::sampleAdded, ui->sample_player, &SamplePlayer::setFocusOn);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::printSnatch(const Sample &snatch)
{
    QString ids_string;
    for (int id: snatch.getIds()) {
        ids_string += QString::number(id) + " ";
    }
    qDebug() << "(" << snatch.getBegin() <<", "<< snatch.getEnd() << ") - " << snatch.getPath() << "(" << ids_string << ")";
}

