#ifndef SLIDERFRAME_H
#define SLIDERFRAME_H

#include <QRectF>
#include <QLineF>
#include <QPainter>

class SliderFrame
{
public:

    enum Mode {
        Frame,
        Bar
    };

    SliderFrame();

    QRectF getRect() const;
    QLine getBar() const;
    int getPosition() const;
    int getBarPosition() const;
    int getFrameBeginPosition() const;
    Mode getMode() const;


    void paint(QPainter& painter) const;

    bool barReachFrameEnd() const;

public slots:
    void setMode(Mode mode);
    void setPosition(int position);
    void setScale(double scale);
    void setLength(int length);
    void setHeight(int height);

private:
    double scale_;
    int height_;

    int length_;

    int position_;
    int bar_position_;

    Mode mode_;
};

#endif // SLIDERFRAME_H
