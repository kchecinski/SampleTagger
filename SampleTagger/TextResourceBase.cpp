#include "TextResourceBase.h"

#include <QMessageBox>
#include <QFileDialog>
#include <QDebug>

TextResourceBase::TextResourceBase()
    :modified_(false)
{
}

TextResourceBase::TextResourceBase(const QString &path)
    :TextResourceBase()
{
    load(path);
}

QString TextResourceBase::getPath() const
{
    return path_;
}

bool TextResourceBase::isModified() const
{
    return modified_;
}

void TextResourceBase::applyModel(QAbstractItemView *view)
{
    view->setModel(&model_);
}

void TextResourceBase::applyModel(QCompleter *completer)
{
    completer->setModel(&model_);
}

void TextResourceBase::loadDialog()
{
    if (modified_) {
        bool if_continue = modifiedFileDialog();
        if (!if_continue)
            return;
    }
    QString path = QFileDialog::getOpenFileName(nullptr, "Open keywords base", QString(""), fileFormat());
    load(path);
}

bool TextResourceBase::load(const QString &path)
{
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(nullptr, "File error", file.errorString());
        return false;
    }
    parseFile(file);
    emit loaded();
    modified_ = false;
    path_ = path;
    return true;
}

bool TextResourceBase::save()
{
    return save(path_);
}

bool TextResourceBase::save(const QString &path)
{
    QFile file(path);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::information(nullptr, "File error", file.errorString());
        return false;
    }
    saveFile(file);
    emit saved();
    modified_ = false;
    path_ = path;
    return true;
}

int TextResourceBase::find(const QString &string)
{
    QRegExp regexp = QRegExp(string,
                             Qt::CaseSensitivity::CaseSensitive,
                             QRegExp::PatternSyntax::FixedString);
    auto idxs = model_.match(model_.index(0,0), Qt::DisplayRole, string);
    if (idxs.isEmpty())
        return -1;
    else
        return idxs.at(0).row();
}

int TextResourceBase::size() const
{
    return model_.rowCount();
}

void TextResourceBase::setStringList(const QStringList &string_list)
{
    model_.setStringList(string_list);
}

void TextResourceBase::addString(const QString &string)
{
    int pos = model_.rowCount();
    addString(string, pos);
}

QStringList TextResourceBase::getStringList() const
{
    return model_.stringList();
}

void TextResourceBase::addString(const QString &string, int position)
{
    model_.insertRow(position);
    auto idx = model_.index(position);
    model_.setData(idx, string);
    modified_ = true;
    emit elementAdded(string);
}

bool TextResourceBase::saveAsDialog()
{
    QString path = QFileDialog::getSaveFileName(nullptr, "Save keywords base", QString(""), fileFormat());
    return save(path);
}

QString TextResourceBase::fileFormat() const
{
    static QString format("Text files (*.txt)");
    return format;
}

bool TextResourceBase::modifiedFileDialog()
{
    QMessageBox box;
    box.setText("Current base has been modified");
    box.setInformativeText("Do you want to save changes?");
    box.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    box.setDefaultButton(QMessageBox::Save);
    int ret = box.exec();
    switch (ret) {
    case QMessageBox::Save:
        saveAsDialog();
        return true;
    case QMessageBox::Discard:
        return true;
    case QMessageBox::Cancel:
        return false;
    }
    return true;

}

QString TextResourceBase::chooseFile()
{
    return QFileDialog::getSaveFileName(
                nullptr,
                "Select file to save",
                "",
                fileFormat());
}
