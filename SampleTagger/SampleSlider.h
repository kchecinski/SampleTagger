#ifndef SAMPLESLIDER_H
#define SAMPLESLIDER_H

#include <QWidget>
#include <QMediaPlayer>

#include "SliderFrame.h"

class SampleSlider : public QWidget
{
    Q_OBJECT

public:

    enum Mode {
        Free,
        Frame
    };

    SampleSlider(QWidget *parent = nullptr);

    QSize sizeHint() const override;

    const SliderFrame& getFrame() const;

    int getFrameBeginPosition() const;

protected:
    void paintEvent(QPaintEvent *event) override;
    void paintSignal(QPainter& painter);

public slots:
    void setSampleLength(int length);
    void setMode(SliderFrame::Mode mode);
    void setPosition(int position);
    void setFrameLength(int length);
    void enableFrame(bool enable);
    void setState(QMediaPlayer::State state);

signals:
    void endOfFrameReached();

private:
    void setFrameWidth(int width);

    Mode mode_;

    SliderFrame frame_;

    bool show_frame_;
};

#endif // SAMPLESLIDER_H
