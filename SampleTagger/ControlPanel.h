#ifndef CONTROLPANEL_H
#define CONTROLPANEL_H

#include <QWidget>
#include <QMediaPlayer>

namespace Ui {
class ControlPanel;
}

class ControlPanel : public QWidget
{
    Q_OBJECT

public:
    explicit ControlPanel(QWidget *parent = nullptr);
    ~ControlPanel();

    QMediaPlayer::State state() const;
    int volume() const;
    int frameLength() const;

private:
    Ui::ControlPanel *ui;

    void assignShortcuts();

public slots:
    void setState(QMediaPlayer::State new_state);
    void setSampleLength(int length);
    void updateManualSlider(int position);
    void samplePlayed();

signals:
    void play();
    void pause();
    void next();
    void previous();
    void windBack();
    void windForward();
    void alignFrame();
    void playFrame();
    void clipFrame();

    void manualSliderMoved(int position);

    void volumeChange(int volume);
    void frameLengthChanged(int length);

private slots:
    void playClicked();
    void handleManualSliderMove(int position);

private:
    QMediaPlayer::State state_;

};

#endif // CONTROLPANEL_H
