#ifndef TAGGEDSNACHESMODEL_H
#define TAGGEDSNACHESMODEL_H

#include <QStringListModel>
#include <QScopedPointer>
#include <QMultiMap>

#include "TextResourceBase.h"
#include "Sample.h"

class TaggedSamplesBase : public TextResourceBase
{
    Q_OBJECT


public:
    static const QString separator;
    static const QString ids_separator;

    TaggedSamplesBase();
    TaggedSamplesBase(const QString& path);

    void add(const Sample& sample);

signals:

private:
    QList<Sample> samples_;

    void parseFile(QFile &file);
    void saveFile(QFile &file);

//    QString fileFormat() const;
};

#endif // TAGGEDSNACHESMODEL_H
