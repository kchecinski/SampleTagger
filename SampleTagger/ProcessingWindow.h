#ifndef PROCESSINGWINDOW_H
#define PROCESSINGWINDOW_H

#include <QWidget>
#include "SampleProcess.h"

namespace Ui {
class ProcessingWindow;
}

class ProcessingWindow : public QWidget
{
    Q_OBJECT

public:

    explicit ProcessingWindow(QWidget *parent = nullptr);
    ~ProcessingWindow();

    int getSamplesCount() const;

    void connectProcess(const SampleProcess* process);
    void disconnectProcess();


public slots:

    void setProcessedSamples(int count);
    void setElementsCount(int count);
    void startStage(const QString& description);
    void finishProcessing();

private slots:
    void abortProcess();


private:
    Ui::ProcessingWindow *ui;
    const SampleProcess* process_;
    int samples_count_;
    int stage_;

    static QString getStageText(int stage);
};

#endif // PROCESSINGWINDOW_H
