#ifndef TAGGER_H
#define TAGGER_H

#include <QWidget>
#include <QCompleter>
#include "KeywordsBase.h"
#include "Sample.h"
#include "TaggedSamplesBase.h"
#include "KeywordsBrowser.h"
#include "ProcessingWindow.h"
#include "SampleEditor.h"

namespace Ui {
class Tagger;
}

class Tagger : public QWidget
{
    Q_OBJECT

public:
    explicit Tagger(QWidget *parent = nullptr);
    ~Tagger();

private slots:
    void onOpenKeywordsBtnClicked();
    void handleKeywordConfirmed(int id);
    void handleEmptyKeywordInput();
    void handleAddInfoConfirmed();
    void handleSampleDoubleClick(const QModelIndex &index);
    void updateActiveSample();
    void deleteActiveSample();

public slots:
    void setCurrentSnatch(const Sample& snatch);
    void clipSamples();

signals:
    void undefinedKeywordUsed(QString keyword);
    void sampleChosen(const Sample& snatch);
    void sampleAdded();

private:
    Ui::Tagger *ui;
    KeywordsBase keyword_base_;
    Sample* current_snatch_;
    Sample chosen_sample_;
    TaggedSamplesBase tagged_samples_;



    KeywordsBrowser keywords_browser_;
    SampleEditor samples_editor_;

    ProcessingWindow* processing_window_;
};

#endif // TAGGER_H
