#ifndef KEYWORDSBROWSER_H
#define KEYWORDSBROWSER_H

#include <QWidget>

#include "KeywordsBase.h"

namespace Ui {
class KeywordsBrowser;
}

class KeywordsBrowser : public QWidget
{
    Q_OBJECT

public:
    explicit KeywordsBrowser(KeywordsBase& keywords, QWidget *parent = nullptr);
    ~KeywordsBrowser();

private slots:
    void addKeyword();
private:
    Ui::KeywordsBrowser *ui;

    KeywordsBase& keywords_;
};

#endif // KEYWORDSBROWSER_H
