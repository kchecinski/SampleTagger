#include "KeywordsBrowser.h"
#include "ui_KeywordsBrowser.h"

#include <QMessageBox>

KeywordsBrowser::KeywordsBrowser(KeywordsBase &keywords, QWidget *parent)
    :QWidget(parent)
    ,ui(new Ui::KeywordsBrowser)
    ,keywords_(keywords)
{
    ui->setupUi(this);
    connect(ui->ok_btn, &QAbstractButton::clicked, this, &QWidget::close);
    connect(ui->lineEdit, &QLineEdit::returnPressed, this, &KeywordsBrowser::addKeyword);
//    ui->keywords_view->setModel(&keywords_.getModel());
    keywords_.applyModel(ui->keywords_view);
}


KeywordsBrowser::~KeywordsBrowser()
{
    delete ui;
}

void KeywordsBrowser::addKeyword()
{
    QString keyword = ui->lineEdit->text();
    if (keyword.size() == 0) {
        QMessageBox::information(nullptr, "Keyword adding", "Keyword can't be empty");
        return;
    }

    if (keywords_.find(keyword) != -1) {
        QMessageBox::information(nullptr, "Keyword adding", "Such keyword already exist");
        return;
    }
    keywords_.addString(keyword);
    ui->lineEdit->clear();


    
}
