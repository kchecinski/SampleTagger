#include "SampleSlider.h"

#include <QPoint>
#include <QPainter>
#include <QDebug>

SampleSlider::SampleSlider(QWidget *parent)
    :QWidget (parent)
    ,mode_(Mode::Free)
    ,show_frame_ (false)
{
}

void SampleSlider::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    frame_.setHeight(height());
    QPainter painter(this);
    painter.drawRect(QRect(0, 0, width()-1, height()-1));
    if (show_frame_)
        frame_.paint(painter);
}

void SampleSlider::setSampleLength(int length)
{
    frame_.setScale(static_cast<double>(width()) / length);
}

void SampleSlider::setMode(SliderFrame::Mode mode)
{
    frame_.setMode(mode);
    update();
}

void SampleSlider::setPosition(int position)
{
    frame_.setPosition(position);
    if (frame_.getMode() == SliderFrame::Mode::Bar && frame_.barReachFrameEnd()) {
        frame_.setMode(SliderFrame::Mode::Frame);
        emit endOfFrameReached();
    }
    update();
}

void SampleSlider::setFrameLength(int length)
{
    frame_.setLength(length);
    update();
}

void SampleSlider::enableFrame(bool enable)
{
    show_frame_ = enable;
    update();
}

void SampleSlider::setState(QMediaPlayer::State state)
{
    enableFrame(state != QMediaPlayer::State::StoppedState);
}


QSize SampleSlider::sizeHint() const
{
    return QSize(450, 90);
}

const SliderFrame &SampleSlider::getFrame() const
{
    return frame_;
}

int SampleSlider::getFrameBeginPosition() const
{
    return frame_.getFrameBeginPosition();
}

