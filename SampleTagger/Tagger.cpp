#include "Tagger.h"
#include "ui_Tagger.h"

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>

#include "SampleProcess.h"

Tagger::Tagger(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Tagger),
    current_snatch_(nullptr),
    chosen_sample_("", 0, 0),
    keywords_browser_(keyword_base_, nullptr)
{
    ui->setupUi(this);

    connect(ui->open_keywords_btn, &QAbstractButton::clicked, &keyword_base_, &KeywordsBase::loadDialog);
    connect(ui->save_keywords_btn, &QAbstractButton::clicked, &keyword_base_, &KeywordsBase::saveAsDialog);
    connect(ui->browse_keywords_btn, &QAbstractButton::clicked, &keywords_browser_, &KeywordsBrowser::show);

    connect(ui->open_samples_btn, &QAbstractButton::clicked, &tagged_samples_, &TaggedSamplesBase::loadDialog);
    connect(ui->save_samples_btn, &QAbstractButton::clicked, &tagged_samples_, &TaggedSamplesBase::saveAsDialog);
    connect(ui->clip_btn, &QAbstractButton::clicked, this, &Tagger::clipSamples);

    ui->keyword_input->setKeywordsBase(&keyword_base_);
    connect(ui->keyword_input, &LabelInput::keywordConfirmed, this, &Tagger::handleKeywordConfirmed);
    connect(ui->keyword_input, &LabelInput::emptyInputConfirmed, this, &Tagger::handleEmptyKeywordInput);

    ui->keyword_input->setReadOnly(true);

    connect(ui->add_info_input, &QLineEdit::returnPressed, this, &Tagger::handleAddInfoConfirmed);
    connect(ui->tagged_snatches_view, &QListView::doubleClicked, this, &Tagger::handleSampleDoubleClick);

    ui->tagged_snatches_view->setEditTriggers(QAbstractItemView::NoEditTriggers);

    tagged_samples_.applyModel(ui->tagged_snatches_view);

    samples_editor_.setKeywordsBase(&keyword_base_);
}

Tagger::~Tagger()
{
    if (processing_window_)
        delete processing_window_;
    delete ui;
}

void Tagger::onOpenKeywordsBtnClicked()
{
    QString path = QFileDialog::getOpenFileName(this, "Open samples", QString(""), "Keyword base files (*.kwb)");
    keyword_base_.load(path);

}

void Tagger::handleKeywordConfirmed(int id)
{
    current_snatch_->addId(id);
}

void Tagger::handleEmptyKeywordInput()
{
    ui->keyword_input->setReadOnly(true);
    ui->add_info_input->setFocus();
    ui->add_info_input->setReadOnly(false);
}


void Tagger::handleAddInfoConfirmed()
{
    if (ui->add_info_input->isReadOnly())
        return;
    QString info = ui->add_info_input->text();
    QString speaker_info = ui->speaker_info_input->text();
    if (!speaker_info.isEmpty())
        info = speaker_info + ":" + info;
    ui->add_info_input->setReadOnly(true);
    ui->keyword_input->setText("");
    ui->add_info_input->setText("");
    current_snatch_->setAdditionalInfo(info);
    tagged_samples_.add(Sample(*current_snatch_));
    emit sampleAdded();
}

void Tagger::handleSampleDoubleClick(const QModelIndex& index)
{
    QString sample_info = ui->tagged_snatches_view->model()->data(index).toString();
    chosen_sample_ = Sample::fromString(sample_info, TaggedSamplesBase::separator, TaggedSamplesBase::ids_separator);
    samples_editor_.setSample(chosen_sample_, index);
    samples_editor_.show();
    connect(&samples_editor_, &SampleEditor::okButtonClicked, this, &Tagger::updateActiveSample);
    connect(&samples_editor_, &SampleEditor::deleteButtonClicked, this, &Tagger::deleteActiveSample);
    emit sampleChosen(chosen_sample_);
}

void Tagger::updateActiveSample()
{
    QModelIndex index = samples_editor_.getIndex();
    int start = samples_editor_.getStart();
    int end = samples_editor_.getEnd();
    QSet<int> labels = samples_editor_.getLabels();
    QString add_info = samples_editor_.getAdditionalInfo();
    chosen_sample_.setIds(labels);
    chosen_sample_.setBegin(start);
    chosen_sample_.setEnd(end);
    chosen_sample_.setAdditionalInfo(add_info);
    ui->tagged_snatches_view->model()->setData(index, chosen_sample_.toString(TaggedSamplesBase::separator, TaggedSamplesBase::ids_separator));
    samples_editor_.close();
}

void Tagger::deleteActiveSample()
{
    QModelIndex index = samples_editor_.getIndex();
    ui->tagged_snatches_view->model()->removeRow(index.row());
    samples_editor_.close();
}

void Tagger::setCurrentSnatch(const Sample &snatch)
{
    if (current_snatch_ != nullptr) {
        delete current_snatch_;
    }
    if (keyword_base_.size() == 0) {
        QMessageBox::warning(nullptr, "Warning", "Keywords list is empty. Load keywords base or add some keywords in browser.");
    }
    current_snatch_ = new Sample(snatch);
    ui->keyword_input->setReadOnly(false);
    ui->keyword_input->setFocus();

}

void Tagger::clipSamples()
{
    if (tagged_samples_.isModified()) {
        QMessageBox::information(nullptr, "List has been modified", "You have to save samples info before start this procedure");
        if (!tagged_samples_.saveAsDialog()) {
            QMessageBox::warning(nullptr, "Error", "Saving failed, procedure aborted");
            return;
        }
    }

    QString base_path = tagged_samples_.getPath();

    if (tagged_samples_.size() == 0 || base_path.isEmpty()) {
        QMessageBox::information(nullptr, "", "List is empty");
        return;
    }

    processing_window_ = new ProcessingWindow();

    static QString working_directory = QCoreApplication::applicationDirPath() + "/../SampleTagger/spect/";
    static QString python_path = "/home/jedikarix/anaconda3/bin/python";


    SampleProcess* process = new SampleProcess(base_path, python_path, working_directory);
    processing_window_->show();
    processing_window_->connectProcess(process);
    //TODO Add environment configuration in file
    process->start();

//    processing_window_.disconnectProcess();
}

