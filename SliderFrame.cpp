#include "SliderFrame.h"

#include <QDebug>

SliderFrame::SliderFrame()
    :scale_(0)
    ,height_(0)
    ,mode_(SliderFrame::Mode::Frame)
{}

QRectF SliderFrame::getRect() const
{
    double x = (position_ - length_) * scale_;
    double w = length_ * scale_;
    return QRectF(x, 0, w, height_);
}

QLine SliderFrame::getBar() const
{
    int x = static_cast<int>(bar_position_ * scale_);
    return QLine(x, 0, x, height_);
}

int SliderFrame::getPosition() const
{
    return position_;
}

int SliderFrame::getBarPosition() const
{
    return bar_position_;
}

int SliderFrame::getFrameBeginPosition() const
{
    return position_ - length_;
}

SliderFrame::Mode SliderFrame::getMode() const
{
    return mode_;
}

void SliderFrame::setMode(SliderFrame::Mode mode)
{
    mode_ = mode;
    if (mode == SliderFrame::Mode::Bar)
        bar_position_ = position_ - length_;
    else
        bar_position_ = 0;
}

void SliderFrame::setPosition(int position)
{
    if (mode_ == SliderFrame::Mode::Bar)
        bar_position_ = position;
    else
        position_ = position;
}

void SliderFrame::setScale(double scale)
{
    scale_ = scale;
}

void SliderFrame::setLength(int length)
{
    length_ = length;
}

void SliderFrame::setHeight(int height)
{
    height_ = height;
}

void SliderFrame::paint(QPainter &painter) const
{
    QRectF rect = getRect();
    painter.fillRect(rect, QColor(150, 150, 150));
    painter.drawRect(rect);
    if (mode_ == SliderFrame::Mode::Bar) {
        QLine bar = getBar();
        QPen pen(QColor(50, 200, 50));
        pen.setWidth(2);
        painter.setPen(pen);
        painter.drawLine(bar);
    }
}

bool SliderFrame::barReachFrameEnd() const
{
    return bar_position_ >= position_;
}
