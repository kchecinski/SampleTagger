#ifndef TEXTRESOURCEBASE_H
#define TEXTRESOURCEBASE_H

#include <QObject>
#include <QStringListModel>
#include <QAbstractItemView>
#include <QCompleter>
#include <QFile>

class TextResourceBase : public QObject
{
    Q_OBJECT

public:
    TextResourceBase();
    TextResourceBase(const QString& path);

    QString getPath() const;
    bool isModified() const;

    void applyModel(QAbstractItemView* view);
    void applyModel(QCompleter* completer);

public slots:
    void loadDialog();
    bool load(const QString& path);
    bool saveAsDialog();
    bool save();
    bool save(const QString& path);
    int find(const QString& string);
    int size() const;
    void addString(const QString& string);
    QStringList getStringList() const;

signals:
    void loaded();
    void saved();
    void elementAdded(const QString& string);

protected:
    void setStringList(const QStringList& string_list);
    void addString(const QString& string, int position);
    void removeString(int position);

private:
    bool modified_;
    QString path_;

    QStringListModel model_;

    virtual void parseFile(QFile& file) = 0;
    virtual void saveFile(QFile& file) = 0;
    virtual QString fileFormat() const;

    bool modifiedFileDialog();
    QString chooseFile();
};

#endif // TEXTRESOURCEBASE_H
