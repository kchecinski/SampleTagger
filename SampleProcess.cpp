#include "SampleProcess.h"

#include <QDebug>
#include <QThread>

SampleProcess::SampleProcess(const QString &base_path, const QString& python_path, const QString& working_path)
    :base_path_(base_path)
    ,python_path_(python_path)
    ,working_path_(working_path)
{}

SampleProcess::~SampleProcess()
{
    if (process_)
        delete process_;
}

bool SampleProcess::start()
{
    static QString script_path = "sample_processing.py";

    process_ = new QProcess(this);
    connect(process_, &QProcess::readyReadStandardOutput, this, &SampleProcess::parseOutput);
    connect(process_, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &SampleProcess::closeProcess);

    QStringList params = QStringList() << script_path << base_path_;
    process_->setArguments(params);
    process_->setProgram(python_path_);
    process_->setWorkingDirectory(working_path_);
    process_->start();
}

void SampleProcess::abort()
{
    if (!process_)
        return;
    process_->terminate();
//    delete process_;
}

void SampleProcess::parseOutput()
{
    QStringList output = QString(process_->readAllStandardOutput()).split('\n');
    output.removeLast();
    qDebug() << output;

    for (auto it = output.begin(); it != output.end(); ++it) {
        QStringList parts = (*it).split(":", QString::SplitBehavior::KeepEmptyParts);
        if (parts.size() != 2)
            continue;
        if (parts[0] == "element") {
            int value = parts[1].toInt();
            emit sampleProcessed(value);
        }
        else if (parts[0] == "count") {
            int value = parts[1].toInt();
            emit setElementsCount(value);
        }
        else if (parts[0] == "stage") {
            emit startStage(parts[1]);
            continue;
        }
        else if (parts[0] == "end") {
            emit processingFinished();
        }

    }
}

void SampleProcess::closeProcess(int code, QProcess::ExitStatus status)
{
    process_->close();
    delete process_;
}
