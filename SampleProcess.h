#ifndef SAMPLEPROCESS_H
#define SAMPLEPROCESS_H

#include <QProcess>
#include <QMap>

class SampleProcess: public QObject
{
    Q_OBJECT

public:
    SampleProcess(const QString &base_path, const QString &python_path, const QString &working_directory);
    ~SampleProcess();

    bool start();
    bool waitForFinished();


signals:
    void setElementsCount(int count);
    void startStage(QString description);
    void sampleProcessed(int id);
    void processingFinished();

public slots:
    void abort();

private slots:
    void parseOutput();    
    void closeProcess(int code, QProcess::ExitStatus status);

private:
    QString base_path_;
    QString python_path_;
    QString working_path_;
    QProcess* process_;
};

#endif // SAMPLEPROCESS_H
