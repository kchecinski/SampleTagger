import numpy as np


class Preemphasis:
    def __init__(self, phi=0.95):
        self.phi = phi

    def preemphasis(self, signal):
        signal.y = preemphasis(signal.y, self.phi)
        return signal


def preemphasis(signal, phi):
    return np.append(signal[0], signal[1:] - phi * signal[:-1])
