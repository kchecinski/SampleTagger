import sys
from load_experiments import load_experiments


def run_experiments(experiments_path):
    runners = load_experiments(experiments_path)

    for runner in runners:
        runner.run()


if __name__ == "__main__":
    path = sys.argv[1]
    run_experiments(path)
