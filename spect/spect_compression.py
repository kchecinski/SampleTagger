from spect import Spectrogram


class BandAverage:
    def __init__(self, ratio, first_band=1, mean='geom', amplif=1, clip_band=24000, ret_full_spectr=False):
        self.ratio = ratio
        self.first_band = first_band
        self.mean = mean
        self.amplif = amplif
        self.ret_full_sepctr = ret_full_spectr
        self.clip_band = clip_band

    def avg_band(self, spectrum):
        s, z = Spectrogram.avg_band(spectrum,
                                    ratio=self.ratio,
                                    first_band=self.first_band,
                                    mean=self.mean,
                                    amplif=self.amplif,
                                    clip_band=self.clip_band)
        if self.ret_full_sepctr:
            return z, s
        else:
            return z
