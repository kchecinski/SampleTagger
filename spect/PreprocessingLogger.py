import sys
from Task import Task


class PreprocessingLogger:

    def __init__(self):
        self.processed_samples = 0
        self.count = 0
        self.finished = False

    def start_stage(self, task: Task):
        if self.finished is True:
            return
        self.processed_samples = 0
        print('stage:' + task.description)
        sys.stdout.flush()

    def next_sample(self):
        if self.finished is True or self.processed_samples >= self.count:
            return
        self.processed_samples += 1
        print('element:' + str(self.processed_samples))
        sys.stdout.flush()

    def elements_count(self, count):
        if self.finished is True:
            return
        self.count = count
        print('count:' + str(count))
        sys.stdout.flush()

    def finish(self):
        self.finished = True
        print('end:')
        sys.stdout.flush()
