from random import sample, choice, choices, random, randint
import numpy as np
from copy import copy


class Dataset:

    def __init__(self, samples=None, name="", train=False, epoch_size=100):
        self.samples = {}
        self.name = name
        self.train = train
        self.epoch_size = epoch_size
        if samples is not None:
            self.add_samples(*zip(*samples))

    def add_sample(self, x: np.ndarray, y: np.ndarray):
        for label in y:
            if label not in self.samples.keys():
                self.samples[label] = []
            self.samples[label].append((x, set(y)))

    def add_samples(self, xs, ys):
        for x, y in zip(xs, ys):
            self.add_sample(x, y)

    def input_size(self):
        return list(self.samples[0][0][0].shape)

    def output_size(self):
        return 1

    def get_diff_pair(self):
        keys = list(self.samples.keys())
        while True:
            labels = sample(keys, 2)
            samples = [choice(self.samples[label]) for label in labels]
            if has_disjoint_labels(samples[0], samples[1]):
                return samples[0][0], samples[1][0], 1

    def get_diff_pair_labels(self):
        keys = list(self.samples.keys())
        while True:
            labels = sample(keys, 2)
            samples = [choice(self.samples[label]) for label in labels]
            if has_disjoint_labels(samples[0], samples[1]):
                return samples[0][0], samples[1][0], 1, [sample[0][1], sample[1][1]]

    def get_same_pair(self):
        keys = list(self.samples.keys())
        label = choice(keys)
        samples = [sampl[0] for sampl in choices(self.samples[label], k=2)]
        return samples[0], samples[1], 0

    def get_same_pair_labels(self):
        keys = list(self.samples.keys())
        label = choice(keys)
        samples = [sampl[0] for sampl in choices(self.samples[label], k=2)]
        return samples[0], samples[1], 0, label

    def get_pair(self):
        indices = sample(range(len(self.samples)), 2)
        return self.samples[indices[0]], self.samples[indices[1]]

    def get_pairs_minibatch_labels(self, size, threshold=0.5):
        pairs_types = [0 if random() < threshold else 1 for _ in range(size)]
        minibatch_samples = [self.get_diff_pair_labels() if v is 0
                             else self.get_same_pair_labels() for v in pairs_types]

        x1s = []
        x2s = []
        ys = []
        ls = []
        for s1, s2, y, l in minibatch_samples:
            x1s.append(s1)
            x2s.append(s2)
            ys.append(np.array([y]))
            ls.append(l)
        return np.stack(x1s), np.stack(x2s), np.stack(ys), ls

    def get_pairs_minibatch(self, size, threshold=0.5):
        pairs_types = [0 if random() < threshold else 1 for _ in range(size)]
        minibatch_samples = [self.get_diff_pair() if v is 0
                             else self.get_same_pair() for v in pairs_types]

        x1s = []
        x2s = []
        ys = []
        for s1, s2, y in minibatch_samples:
            x1s.append(s1)
            x2s.append(s2)
            ys.append(np.array([y]))
        return np.stack(x1s), np.stack(x2s), np.stack(ys)

    def keys(self):
        return self.samples.keys()


class GenerativeDataset(Dataset):
    def __init__(self, out_width, samples=None, name="", train=False, epoch_size=100):
        Dataset.__init__(self, samples, name, train, epoch_size)
        self.out_width = out_width

    def get_diff_pair(self, with_labels=False):
        sample1, sample2, out = Dataset.get_diff_pair(self)
        sample1 = random_clip_input(sample1, self.out_width)
        sample2 = random_clip_input(sample2, self.out_width)
        return sample1, sample2, out

    def get_diff_pair_labels(self):
        sample1, sample2, out, labels = Dataset.get_diff_pair_labels(self)
        sample1 = random_clip_input(sample1, self.out_width)
        sample2 = random_clip_input(sample2, self.out_width)
        return sample1, sample2, out, labels

    def get_same_pair(self, with_labels=False):
        sample1, sample2, out = Dataset.get_same_pair(self)
        sample1 = random_clip_input(sample1, self.out_width)
        sample2 = random_clip_input(sample2, self.out_width)
        return sample1, sample2, out

    def get_same_pair_labels(self):
        sample1, sample2, out, labels = Dataset.get_same_pair_labels(self)
        sample1 = random_clip_input(sample1, self.out_width)
        sample2 = random_clip_input(sample2, self.out_width)
        return sample1, sample2, out, labels

    def input_size(self):
        size = list(self.samples[0][0][0].shape)
        size[1] = self.out_width
        return size


def scale_input(x):
    return x / 255


def has_disjoint_labels(s1, s2):
    return int(len(s1[1] & s2[1]) == 0)


def calculate_samples_distribution(samples):
    superventions = {}
    for sampl in samples:
        for label in sampl[1]:
            superventions[label] = superventions.get(label, 0) + 1
    return list(superventions.items())


def find_samples(samples, labels: set):
    return [sampl for sampl in samples if sampl[1] in labels]


def divide_by_labels(samples, labels: set):
    flags = [len(set(sampl[1]) & labels) != 0 for sampl in samples]
    has_labels = [sampl for sampl, flag in zip(samples, flags) if flag]
    hasnt_labels = [sampl for sampl, flag in zip(samples, flags) if not flag]
    return has_labels, hasnt_labels


def sectionalize_rare_samples(samples, rare_count=0):
    if rare_count != 0:
        distribution = calculate_samples_distribution(samples)
        least_labels = [freq[0] for freq in sorted(distribution, key=lambda x: x[1])[:rare_count]]
        rare_samples, common_samples = divide_by_labels(samples, set(least_labels))
    else:
        common_samples = samples
        rare_samples = []
    return rare_samples, common_samples


def k_fold(samples, part, k):
    segment_size = int(len(samples) / k)
    test_begin = segment_size * part
    test_end = test_begin + segment_size
    train_samples = samples[:test_begin] + samples[test_end:]
    test_samples = samples[test_begin:test_end]

    return train_samples, test_samples


def random_clip_input(input_arr, out_width):
    beg = randint(0, input_arr.shape[1] - out_width)
    return copy(input_arr[:, beg:beg + out_width, :])

