from pydub import AudioSegment
import sys


def slice_record(path, duration, out_path):
    record = AudioSegment.from_mp3(path)
    print('loaded')
    for i, t in enumerate(range(0, len(record), duration)):
        print(i)
        curr_slice = record[t, t + duration]
        curr_slice.export(out_path + "_" + str(i) + ".mp3", format='mp3')


if __name__ == "__main__":
    path = sys.argv[1]
    slice_dur = sys.argv[2]
    out_path = sys.argv[3]
    slice_record(path, slice_dur, out_path)
