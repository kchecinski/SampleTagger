import numpy as np
import pandas as pd
from os import listdir
from os.path import isfile, join


def analyse(path, avg_size=20, sep='\t', line_terminator='\n'):
    print(path)
    values = pd.read_csv(path, sep=sep, lineterminator=line_terminator).values[:, 1:]
    length = np.shape(values)[0]

    avg_data = np.array([np.average(values[i:i+avg_size, :], axis=0) for i in range(length-avg_size)])
    maxs = np.max(avg_data, axis=0)
    mins = np.min(avg_data, axis=0)
    argmaxs = np.argmax(avg_data, axis=0)
    argmins = np.argmin(avg_data, axis=0)
    return [mins[0], maxs[1], mins[2], maxs[3],
            argmins[0], argmaxs[1], argmins[2], argmaxs[3],
            avg_data[argmins[0], 1], avg_data[argmins[2], 3]]


def analyse_dir(dir, output_filename):
    logs = [join(dir, f) for f in listdir(dir) if isfile(join(dir, f)) and f.endswith('.log')]
    with open(output_filename, 'w') as output:
        for log_filename in logs:
            result = [log_filename] + analyse(log_filename)
            output.write('\t'.join([str(res) for res in result]) + '\n')
