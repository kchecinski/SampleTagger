from sample_clipping import ClipTask, read_samples_info, ExpandSampleTask
from stft import Stft, Melspectrogram
from spect import Spectrogram
from transformation import TransformGenerator
from spect_compression import BandAverage
from SavingManager import SavingManager
from data_serialization import SampleSaver
from audio_container import AudioContainer
from preemphasis import Preemphasis
from normalization import SignalNormalizationTask, SpectLogNormalizationTask

from PreprocessingLogger import PreprocessingLogger, Task
from preprocessing_config import load_config

import sys
import itertools


# expanding_task = Task(ExpandSampleTask(config["expand"], config["file_length"]).expand, "expand frames")
# clipping_task = Task(ClipTask(audio_container).clip, "samples clipping")

def expand_samples(samples_info, config):
    expanding_task = Task(ExpandSampleTask(config["expand"], config["file_length"]).expand, "expand frames")
    return process_task_nolog(samples_info, expanding_task)


def load_signals(samples_info):
    audio_container = AudioContainer(True)
    paths = set([inf.path for inf in samples_info])
    audio_container.load(paths)

    clipping_task = Task(ClipTask(audio_container).clip, "samples clipping")

    return process_task_nolog(samples_info, clipping_task)


def preprocess(signals, config, max_batch=None, multiply=False):

    single = False

    if not isinstance(signals, list):
        signals = [signals]
        single = True

    if config["transform"] == "stft":
        stft = Stft(**config["transform_params"])
    else:
        stft = Melspectrogram(**config["transform_params"])

    trans_generator = TransformGenerator(stretch_ranges=[config["x_stretch"], config["y_stretch"]],
                                         out_width=config["frame_clip"],
                                         noise_params=config["noise"],
                                         multiplier=config["trans_mult"])

    compression = BandAverage(ratio=config["compr_ratio"],
                              mean='geom',
                              ret_full_spectr=config["istft"],
                              clip_band=config["compr_clip_band"])
    saving_manager = SavingManager('/home/jedikarix/Dokumenty/Inżynierka/SampleTagger/spect/results2/')
    normalization_task = None
    normalization_params = config.get('normalization', None)
    if normalization_params is not None:
        normalization_task = Task(SignalNormalizationTask(**config["normalization"]).normalize, "signals f0 normalization")
    preemphasis_task = Task(Preemphasis(config["preemphasis"]).preemphasis, "signals pre-emphasing")
    stft_computing_task = Task(stft.transform, "computing short-time Fourier transform (STFT)")
    real_part_task = Task(Spectrogram.to_absolute, "remove spectra imaginary part")
    transform_task = Task(trans_generator.random_trans, "random transformations generating")
    compression_task = Task(compression.avg_band, "spectra compression")
    # save_task = Task(saving_manager.save_bitmap, "data saving")

    logger = PreprocessingLogger()

    batches = []
    if max_batch is not None:
        assigned = 0
        while assigned < len(signals):
            batches.append(signals[assigned:max(assigned + max_batch, len(signals))])
    else:
        batches = [signals]

    output = []

    for signals in batches:

        signals = process_task(signals, normalization_task, logger)
        signals = process_task(signals, preemphasis_task, logger)
        spectra = process_task(signals, stft_computing_task, logger)
        spectra = process_task(spectra, real_part_task, logger)
        if multiply:
            spectra = process_task(spectra, transform_task, logger)
            spectra = list(itertools.chain.from_iterable(spectra))
        comp_spectra = None
        if config['transform'] == 'stft':
            comp_spectra = process_task(spectra, compression_task, logger)
        elif config['transform'] == 'mel':
            comp_spectra = [spectrum.D for spectrum in spectra]

        if config['istft']:
            inverse_task = Task(Spectrogram.inverse, "computing ISTFT")
            save_task = Task(saving_manager.save_sound, "save sound")
            comp_spectra, spectra = zip(*comp_spectra)
            signals = process_task(spectra, inverse_task, logger)
            process_task(signals, save_task, logger)

        if config['log_norm']:
            lognorm_task = Task(SpectLogNormalizationTask(1, 5).normalize, 'log normalization')
            comp_spectra = process_task(comp_spectra, lognorm_task, logger)

        output = output + comp_spectra

        # saver.save_to_separate_files(comp_spectra)

    logger.finish()
    if single:
        return output[0]
    else:
        return output


def process_task(inp, task, log: PreprocessingLogger):
    if task is None:
        print("task ommited")
        return inp
    out = []
    log.elements_count(len(inp))
    log.start_stage(task)
    for i in inp:
        out.append(task.func(i))
        log.next_sample()
    return out


def process_task_nolog(inp, task):
    return [task.func(i) for i in inp]


def repeat(a, rep):
    return list(itertools.chain.from_iterable([[elem] * rep for elem in a]))


def main():
    samples_info_path = sys.argv[1]
    info = read_samples_info(samples_info_path)
    config_path = sys.argv[2]
    config = load_config(config_path)
    dataset_path = sys.argv[3]
    max_batch = sys.argv[4] if len(sys.argv) > 4 else None

    info = expand_samples(info, config)

    print([(inf.begin, inf.end) for inf in info])
    samples = load_signals(info)
    signals, ids = zip(*samples)
    signals = list(signals)
    ids = list(ids)
    spectrograms = preprocess(signals, config, max_batch)

    saver = SampleSaver(dataset_path)
    mult = config['trans_mult']
    saver.save_to_separate_files(zip(repeat(spectrograms, mult), ids))


if __name__ == '__main__':
    main()
