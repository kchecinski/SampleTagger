from spect import Signal, Spectrogram
import librosa
import numpy as np
from scipy.signal import medfilt, find_peaks
from yin import compute_yin


def frame_length(sr, dur):
    return int(dur / 1000 * sr)


def zero_crossing_rate(signal: Signal, frame_dur):
    frame_len = frame_length(signal.sr, frame_dur)
    zcr = librosa.feature.zero_crossing_rate(signal.y, frame_length=frame_len, hop_length=frame_len)
    return np.reshape(zcr, np.shape(zcr)[1])


def short_time_energy(signal: Signal, frame_dur):
    frame_len = frame_length(signal.sr, frame_dur)
    ste = librosa.feature.rmse(signal.y, frame_length=frame_len, hop_length=frame_len)**2
    return np.reshape(ste, np.shape(ste)[1])


def threshold(feature_seq, weight=5):
    mean = np.mean(feature_seq)
    hist, x = np.histogram(feature_seq, int(len(feature_seq)/10))

    maxima, _ = find_peaks(hist)

    return (weight * x[maxima[0]] + x[maxima[1]]) / (weight + 1) if len(maxima) >= 2 else mean / 2


def identification(signal, zcr_window=20, ste_window=50):

    zcr = zero_crossing_rate(signal, zcr_window)
    ste = short_time_energy(signal, ste_window)
    #
    # zcr = medfilt(medfilt(zcr, 5), 5)
    # ste = medfilt(medfilt(ste, 5), 5)

    t_e = threshold(ste)

    output = []

    for zcr_v, ste_v in zip(zcr, ste):
        if zcr_v < 0.1 and ste_v > t_e:
            output.append(2)
        elif 0.1 < zcr_v < 0.3 and ste_v < t_e:
            output.append(0)
        else:
            output.append(1)
    return output


def stretch(a, out_size):
    in_size = len(a)
    out_xs = [i * in_size/out_size for i in range(out_size)]
    return np.interp(out_xs, range(in_size), a)


def normalize_signal(signal, f0_norm=150, voiced_scale=0.6, unvoiced_scale=0.1):
    ident = identification(signal)
    f0, _, _, _ = compute_yin(signal.y, signal.sr, w_len=512, w_step=256)
    spect = Signal.stft(signal, n_fft=2048)

    ident = [round(val) for val in stretch(ident, spect.D.shape[1])]
    f0 = stretch(f0, spect.D.shape[1])

    for i, f0_v, ident_v in zip(range(len(f0)), f0, ident):
        k = 0.6 if ident_v == 2 else 0.1
        f_shift = k*(f0_v - f0_norm)
        shift = f_shift * np.shape(spect.D)[0] * 2 / signal.sr
        spect.D[:, i] = np.roll(spect.D[:, i], int(round(shift)))
    normalized = Spectrogram.inverse(spect)
    return normalized


class SignalNormalizationTask:
    def __init__(self, f0_norm, voiced_scale=0.6, unvoiced_scale=0.1):
        self.f0_norm = f0_norm
        self.voiced_scale = voiced_scale
        self.unvoiced_scale = unvoiced_scale

    def normalize(self, signal):
        return normalize_signal(signal,
                                f0_norm=self.f0_norm,
                                voiced_scale=self.voiced_scale,
                                unvoiced_scale=self.unvoiced_scale)


def normalize(data, a=0, b=1):
    d_abs = np.absolute(data)
    d_min = np.min(d_abs)
    d_max = np.max(d_abs)
    d_scope = d_max - d_min
    if d_scope == 0:
        return 0
    else:
        return (d_abs - d_min)*(b - a)/d_scope + a


class SpectLogNormalizationTask:
    def __init__(self, min_v, max_v=None):
        self.min_v = min_v
        self.max_v = max_v

    def normalize(self, spect):
        print(np.max(spect))
        return np.log(normalize(spect, self.min_v, self.max_v if self.max_v is not None else np.max(spect)))
