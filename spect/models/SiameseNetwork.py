import tensorflow as tf


class SiameseNetwork:

    def __init__(self, twin_builder, distance_builder):
        self.twin_builder = twin_builder
        self.distance_builder = distance_builder
        self.twin1 = None
        self.twin2 = None
        self.out = None
        self.layers = []

    def build(self, x1, x2):
        with tf.variable_scope('siamese_network', reuse=tf.AUTO_REUSE):
            self.twin1 = self.twin_builder(x1)
            self.twin2 = self.twin_builder(x2)
            ####################################
            # self.out = tf.reduce_sum(tf.abs(tf.subtract(self.twin1, self.twin2)))
            subt = tf.abs(tf.subtract(self.twin1, self.twin2))
            self.out = self.distance_builder(subt)
        self.layers = self.twin_builder.layers + self.distance_builder.layers


