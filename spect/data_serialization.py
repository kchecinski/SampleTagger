import os
from scipy import misc
from spect import array2bitmap
import numpy as np
from collections import defaultdict


class SamplesManager:
    def __init__(self, directory):
        self.directory = directory
        self.samples_dict = defaultdict(list)
        self.samples_last_id = {}
        self.update_samples()

    def update_samples(self):
        files = [name for name in os.listdir(self.directory) if name.split('.')[-1] == 'bmp']
        for name in files:
            splitted = name.split('_')
            label = splitted[0]
            ident = str(splitted[1]).split('_')[0]
            self.samples_dict[label].append(ident)
        for label in self.samples_dict.keys():
            self.samples_last_id[label] = max(self.samples_dict[label])

    def add_sample(self, sample, label):
        ident = self.samples_last_id.get(label, 0) + 1
        path = "{}/{}_{}".format(self.directory, label, ident)
        array2bitmap(sample, path)
        self.samples_last_id[label] = ident

    def add_samples(self, samples):
        for sample, label in samples:
            self.add_sample(sample, label)


class SampleSaver:
    def __init__(self, directory):
        self.i = 0
        self.directory = directory
        if not os.path.exists(directory):
            os.makedirs(directory)

    def save_to_separate_files(self, samples):
        self.i = 0
        for sample, labels in samples:
            path = "{}/{}_{}".format(self.directory, ','.join([str(label) for label in labels]), self.i)
            array2bitmap(sample, path)
            self.i += 1


def load_from_separate_files(directory):
    files = [name for name in os.listdir(directory) if name.split('.')[-1] == 'bmp']
    samples = []
    for name in files:
        labels = [int(label) for label in str(name.split('_')[0]).split(',')]
        sample = misc.imread(name="{}/{}".format(directory, name), mode='L') / 255.0
        samples.append((add_channel_dim(sample), np.array(labels)))
    return samples


def add_channel_dim(array):
    return np.expand_dims(array, axis=2)
