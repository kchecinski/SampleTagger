import tensorflow as tf
from Logger import Logger
from Dataset import Dataset, GenerativeDataset, k_fold
from random import shuffle


class Experiment:

    def __init__(self,
                 minibatch_size,
                 samples,
                 model_params,
                 loss_operator,
                 optimizer,
                 stop_condition=lambda exp: False,
                 show_plots=True,
                 save_plots=None,
                 save_log=False,
                 input_width=None,
                 name=""):
        self.samples = samples
        self.minibatch_size = minibatch_size
        self.model_params = model_params
        self.loss_operator = loss_operator
        self.optimizer = optimizer
        self.stop_condition = stop_condition
        self.show_plots = show_plots
        self.save_plots = save_plots
        self.name = name
        self.input_width = input_width
        self.save_log = save_log

        self.x1, self.x2, self.model = None, None, None
        self.model_out = None
        self.labels = None
        self.loss = None
        self.optimizer_op = None
        self.pred_op = None
        self.pred_error_opt = None

    def build_graph(self):
        tf.reset_default_graph()
        if self.input_width is None:
            input_shape = [self.minibatch_size] + list(self.samples[0][0].shape)
        else:
            sample_shape = list(self.samples[0][0].shape)
            sample_shape[1] = self.input_width
            input_shape = [self.minibatch_size] + sample_shape

        self.x1, self.x2, self.model = self.model_params.build_model(input_shape)
        self.model_out = self.model.out
        self.labels = tf.placeholder(tf.float32, self.model_out.shape, name='labels')
        self.loss = self.loss_operator(self.model_out, self.labels)

        self.optimizer_op = self.optimizer.minimize(self.loss)
        self.pred_op = prediction_opt(self.model_out)
        self.pred_error_opt = prediction_error_opt(self.pred_op, self.labels)

    def run(self, epochs_count, epoch_size, test_size, train_test_ratio=0.9, save_model=None):
        shuffle(self.samples)
        self.build_graph()

        train_samples_count = int(len(self.samples) * train_test_ratio)
        if self.input_width is None:
            train_ds = Dataset(self.samples[:train_samples_count], name="train", train=True, epoch_size=epoch_size)
            test_ds = Dataset(self.samples[train_samples_count:], name="test", train=False, epoch_size=test_size)
        else:
            train_ds = GenerativeDataset(self.input_width, self.samples[:train_samples_count], name="train",
                                         train=True, epoch_size=epoch_size)
            test_ds = GenerativeDataset(self.input_width, self.samples[train_samples_count:], name="test",
                                        train=False, epoch_size=test_size)

        init_op = tf.global_variables_initializer()

        logger = self.run_on_datasets(epochs_count=epochs_count,
                                      init_op=init_op,
                                      datasets=[train_ds, test_ds],
                                      save_model=save_model)

        best_logger = Logger()
        best_logger.add_logs(logger["train error", "test error"].reduce(min))
        best_logger.add_logs(logger["train accuracy", "test accuracy"].reduce(max))

        print('Best result:')
        best_logger.print_last_line()

        logger.generate_plot(keys=["train accuracy", "test accuracy"],
                             plot_name=self.name + " Accuracy", show=self.show_plots, save_path=self.save_plots)
        logger.generate_plot(keys=["train error", "test error"],
                             plot_name=self.name + " Error", show=self.show_plots, save_path=self.save_plots,
                             y_scale='log')
        if self.save_log:
            logger.save_log(file_path=self.name + '.log')

    def run_k_fold(self, epochs_count, epoch_size, test_size, dataset_parts=10):
        shuffle(self.samples)
        self.build_graph()
        k_fold_logger = Logger()
        for i in range(dataset_parts):
            print("Run {}/{}".format(i+1, dataset_parts))
            train_samples, test_samples = k_fold(self.samples, i, dataset_parts)

            if self.input_width is None:
                train_ds = Dataset(train_samples, name="train", train=True, epoch_size=epoch_size)
                test_ds = Dataset(test_samples, name="test", train=False, epoch_size=test_size)
            else:
                train_ds = GenerativeDataset(self.input_width, train_samples, name="train",
                                             train=True, epoch_size=epoch_size)
                test_ds = GenerativeDataset(self.input_width, test_samples, name="test",
                                            train=False, epoch_size=test_size)

            init_op = tf.global_variables_initializer()

            logger = self.run_on_datasets(epochs_count=epochs_count,
                                          init_op=init_op,
                                          datasets=[train_ds, test_ds])

            logger.generate_plot(keys=["train accuracy", "test accuracy"],
                                 plot_name=self.name + str(i) + " Accuracy",
                                 show=self.show_plots, save_path=self.save_plots)
            logger.generate_plot(keys=["train error", "test error"],
                                 plot_name=self.name + str(i) + " Error",
                                 show=self.show_plots, save_path=self.save_plots,
                                 y_scale='log')
            k_fold_logger.add_logs(logger["train error", "test error"].reduce(min))
            k_fold_logger.add_logs(logger["train accuracy", "test accuracy"].reduce(max))

            if logger.get_info("break"):
                break
        mean_error = k_fold_logger.reduce(avg_error)
        print('avg result: \n')
        mean_error.print_last_line()

    def run_on_datasets(self, epochs_count, init_op, datasets, save_model=None):
        saver = tf.train.Saver()

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        with tf.Session(config=config) as sess:

            writer = tf.summary.FileWriter('./graphs', sess.graph)

            sess.run(init_op)

            epochs_logger = Logger()

            try:

                for epoch in range(epochs_count):
                    if self.stop_condition(self) is True:
                        break

                    for dataset in datasets:
                        ds_logger = self.process_dataset(dataset,
                                                         dataset.epoch_size,
                                                         sess,
                                                         learn=dataset.train)

                        epochs_logger.add_logs(ds_logger, prefix=dataset.name + " ")

                    print(epochs_logger.get_line(-1))

            except KeyboardInterrupt:
                epochs_logger.add_value('Broken', True)

            if save_model is not None:
                self.save_model(sess, saver, save_model)

        return epochs_logger

    def process_dataset(self, dataset, minibatches_count, sess, learn=False):
        logger = Logger()
        for _ in range(0, minibatches_count):
            minibatch = dataset.get_pairs_minibatch(self.minibatch_size)
            minibatch_dict = {self.x1: minibatch[0], self.x2: minibatch[1], self.labels: minibatch[2]}
            if learn:
                error, pred_error, _ = sess.run([self.loss,
                                                 self.pred_error_opt,
                                                 self.optimizer_op], minibatch_dict)
            else:
                error, pred_error = sess.run([self.loss, self.pred_error_opt], minibatch_dict)

            logger.add_value("error", error)
            logger.add_value("accuracy", pred_error)

        return logger.reduce(avg_error)

    def save_model(self, sess, saver, path):
        saver.save(sess, path)
        self.model_params.to_json(path + '.json')


def avg_error(log):
    return sum(log) / len(log) if len(log) != 0 else None


def prediction_opt(output):
    return tf.cast(tf.greater(output, 0.5), dtype=tf.float32)


def prediction_error_opt(prediction, labels):
    return tf.reduce_mean(tf.cast(tf.equal(prediction, labels), dtype=tf.float32))
