import sys
from search_engine.user_input import prepare_input
from models.Model import load_model
import json
import tensorflow as tf
import numpy as np
from search_engine.generate_frames import generate_frames
from scipy.signal import medfilt
import matplotlib.pyplot as plt


def main():
    record_path = sys.argv[1]
    sample_path = sys.argv[2]
    config_path = sys.argv[3]
    model_path = sys.argv[4] if len(sys.argv) > 4 else None

    record, sample = prepare_input(record_path=record_path,
                                   sample_path=sample_path,
                                   config_path=config_path)

    search(record, sample, model_path, step_size=2)


one_step_samples = 200


def load_model_info(path):
    model_params_path = path + '.json'
    model_variables_path = path
    with open(model_params_path) as file:
        model_params = json.load(file)
    model = load_model(model_params)
    return model, model_variables_path


def search(record, sample, model_path, step_size):

    sample_width = sample.shape[1]
    record_width = record.shape[1]

    model, model_variables_path = load_model_info(model_path)

    steps = int(np.ceil((record_width - sample_width) / step_size))

    input_shape = [one_step_samples] + list(sample.shape)

    tf.reset_default_graph()

    x1, x2, y = model.build_model(input_shape=input_shape)

    saver = tf.train.Saver()

    samples = np.stack([sample for _ in range(one_step_samples)])

    detection = []

    with tf.Session() as sess:
        writer = tf.summary.FileWriter('./graphs2', sess.graph)
        saver.restore(sess, model_variables_path)

        for i in range(0, steps, one_step_samples):
            print("progress: {0:0.2f}%".format(i*100/steps))
            frames = generate_frames(record=record,
                                     frame_size=sample_width,
                                     frames_count=one_step_samples,
                                     step=i,
                                     step_size=step_size)
            batch_dict = {x1: samples, x2: frames}
            out = sess.run(y.out, batch_dict)
            detection = detection + out.flatten().tolist()

    time_scale = 60*np.arange(0, len(detection))/(record_width/step_size)
    plt.plot(time_scale, detection)
    plt.show()
    result = np.argmax(detection)
    detection = medfilt(detection, 5)
    plt.plot(detection)
    plt.show()
    print(60*result/(record_width/step_size))

    round_detection = [1 if val >= 0.9 else 0 for val in detection]
    plt.plot(round_detection)
    plt.show()
    begin = None
    for i, val in enumerate(round_detection):
        if val == 1:
            if begin is None:
                begin = i
        if val == 0:
            if begin is not None:
                print('{0:0.2f} - {0:0.2f}'.format(60 * begin * step_size / record_width,
                                                   60 * i * step_size / record_width))
                begin = None


if __name__ == '__main__':
    main()
