import numpy as np


def generate_frames(record, frame_size, frames_count, step, step_size):
    record_width = record.shape[1]
    start_pos = step * step_size
    end_pos = start_pos + frames_count * step_size
    last_frame_pos = record_width - frame_size
    frame_shape = list(np.shape(record))
    frame_shape[1] = frame_size
    frames = [record[:, pos: pos + frame_size] for pos in range(start_pos, min(last_frame_pos, end_pos), step_size)]
    if end_pos > last_frame_pos:
        offset = end_pos - last_frame_pos
        if offset < frame_size:
            partial_frame = np.zeros(tuple(frame_shape))
            partial_frame[:, :offset] = record[:, -offset:]
            frames.append(partial_frame)
    for _ in range(len(frames), frames_count):
        frames.append(np.zeros(tuple(frame_shape)))
    return np.stack(frames)
