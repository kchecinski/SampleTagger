from spect import Signal
from sample_processing import preprocess
import json
import numpy as np


def load_config(path):
    with open(path) as f:
        data = json.load(f)
    return data


def clip_signal(signal, length):
    return Signal.clip(signal, 0, length)


def generate_spectr(signal, config, clip=False):

    spectrogram = preprocess(signal, config, multiply=False)
    spectrogram = np.expand_dims(spectrogram, axis=2)
    if clip:
        frame_length = config['frame_clip']
        spectrogram = spectrogram[:, :frame_length]

    return spectrogram


def prepare_input(record_path, sample_path, config_path):
    record = Signal.load(record_path)
    sample = Signal.load(sample_path)
    config = load_config(config_path)

    record_spect = generate_spectr(record, config)
    sample_spect = generate_spectr(sample, config, clip=True)

    return record_spect, sample_spect
