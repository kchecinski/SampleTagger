from spect import Signal
import os


class AudioContainer:

    def __init__(self, verbose):
        self.samples = {}
        self.verbose = verbose

    def load(self, paths: set):
        for i, path in enumerate(paths):
            self.samples[path] = Signal.load(path)
            if self.verbose:
                print('{}/{} loaded'.format(i+1, len(paths)))

    def load_dir(self, directory):
        paths = set()
        for filename in os.listdir(directory):
            if filename.endswith(".mp3") or filename.endswith(".wav"):
                paths.add(directory + filename)
        self.load(paths)



