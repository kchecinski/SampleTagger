from spect import Signal


class Stft:

    def __init__(self, n_fft=1024, hop_length=None, win_length=None, window='hann', center=True):
        self.n_fft = n_fft
        self.hop_length = hop_length
        self.win_length = win_length
        self.window = window
        self.center = center

    def transform(self, signal):
        spectrum = Signal.stft(signal,
                               n_fft=self.n_fft,
                               hop_length=self.hop_length,
                               win_length=self.win_length,
                               window=self.window,
                               center=self.center)
        return spectrum


class Melspectrogram:

    def __init__(self, n_fft=2048, n_mels=128, fmin=0.0, fmax=None):
        self.n_fft = n_fft
        self.n_mels = n_mels
        self.fmin = fmin
        self.fmax = fmax

    def transform(self, signal):
        spectrum = Signal.melspect(signal, n_fft=self.n_fft, n_mels=self.n_mels, fmin=self.fmin, fmax=self.fmax)
        return spectrum
