#ifndef LABELINPUT_H
#define LABELINPUT_H

#include <QLineEdit>
#include <QCompleter>
#include "KeywordsBase.h"

class LabelInput : public QLineEdit
{
    Q_OBJECT

public:
    LabelInput(QWidget *parent = nullptr);
    LabelInput(const QString& contents, QWidget* parent = nullptr);

    virtual ~LabelInput();

    void setKeywordsBase(KeywordsBase* keywords);

signals:
    void emptyInputConfirmed();
    void keywordConfirmed(int id);
    void undefinedKeywordUsed(QString label);

private slots:
    void checkInput();

private:
    QCompleter completer_;
    KeywordsBase* keywords_;

};

#endif // LABELINPUT_H
