#include "Sample.h"

#include <QDebug>

Sample::Sample(QString path, int begin, int end, const QString &info)
   :path_(path)
   ,begin_(begin)
   ,end_(end)
   ,add_info_(info)
{}

Sample::Sample(QUrl url, int begin, int end, const QString &info)
    :path_(urlToPath(url))
    ,begin_(begin)
    ,end_(end)
    ,add_info_(info)
{}

int Sample::getBegin() const
{
    return begin_;
}

int Sample::getEnd() const
{
    return end_;
}

const QString Sample::getPath() const
{
    return path_;
}

QSet<int> Sample::getIds() const
{
    return ids_;
}

QString Sample::getAdditionalInfo() const
{
    return add_info_;
}

void Sample::setBegin(int begin)
{
    begin_ = begin;
}

void Sample::setEnd(int end)
{
    end_ = end;
}

void Sample::setIds(const QSet<int> &ids)
{
    ids_ = ids;
}

void Sample::setAdditionalInfo(const QString &info)
{
    add_info_ = info;
}

void Sample::addId(int id)
{
    ids_.insert(id);
}

QString Sample::toString(const QString &separator, const QString& ids_separator) const
{
    QString ids_str;
    for (int id: ids_) {
        ids_str += QString::number(id) + ids_separator;
    }
    ids_str.chop(1);
    return ids_str + separator + path_ + separator + QString::number(begin_) + separator + QString::number(end_) + separator + add_info_;
}

Sample Sample::fromString(const QString &data, const QString& separator, const QString& ids_separator)
{
    QStringList parts = data.split(separator);
    QStringList ids_str = parts[0].split(ids_separator);
    QSet<int> ids;
    for (QString id_str: ids_str) {
        ids.insert(id_str.toInt());
    }

    QString path = parts[1];
    int start = parts[2].toInt();
    int end = parts[3].toInt();
    QString add_info = parts.length() > 4 ? parts[4] : "";
    Sample sample = Sample(path, start, end, add_info);
    sample.setIds(ids);
    return sample;
}

QString Sample::urlToPath(QUrl url)
{
    QString str = url.toString();
    if (str.startsWith("file:///")) {
        int d = str.at(9) == ':' ? 8 : 7;

        QString res = str.right(str.length() - d);
        return res;
    } else {
        return str;
    }
}

