#ifndef SAMPLEPLAYER_H
#define SAMPLEPLAYER_H

#include <QWidget>
#include <QFileDialog>
#include <QMediaPlaylist>
#include <QMediaPlayer>

#include "SampleSlider.h"
#include "PlaylistModel.h"
#include "Sample.h"


const qint64 WIND_SIZE = 500;

namespace Ui {
class SamplePlayer;
}

class SamplePlayer : public QWidget
{
    Q_OBJECT

public:
    explicit SamplePlayer(QWidget *parent = nullptr);
    ~SamplePlayer();

    void addToPlaylist(const QList<QUrl> urls);
    void addToPlaylist(const QUrl url);

public slots:
    void playSample(const Sample& sample);
    void setFocusOn();

private slots:
    void on_OpenFilesButton_clicked();
    void sampleDoubleClicked(const QModelIndex &index);
    void activeSampleChanged(int index);
    void windBack();
    void windForward();
    void playFrame();    
    void stopFramePlaying();
    void alignPlayerToFrame();
    void exactSnatch();

signals:
    void sliderModeChanged(SampleSlider::Mode mode);
    void snatchGenerated(const Sample& snatch);


private:
    Ui::SamplePlayer *ui;
    PlaylistModel playlist_model_;
    QMediaPlaylist playlist_;
    QList<QString> loaded_paths_;
    QMediaPlayer player_;

};

#endif // SAMPLEPLAYER_H
