import json
import os
from learning import Experiment
from models.Model import load_model
from data_serialization import load_from_separate_files
import tensorflow as tf
import librosa
import numpy as np
import copy


optimizers = {
    "Momentum": tf.train.MomentumOptimizer,
    "Adam": tf.train.AdamOptimizer,
}

losses = {
    "mean_square_error": tf.losses.mean_squared_error
}


def read(struct, key):
    val = struct.get(key, None)
    if val == "None" or val == "none":
        val = None
    return val


def load_experiments(filepath):
    ds_manager = DatasetsManager()
    runners = []
    with open(filepath) as file:
        print(file.read())
        file.seek(0)
        experiments = json.load(file)
    print("Loaded experiments: {}".format(len(experiments)))
    for experiment in experiments:

        model_params = experiment["model"]
        if isinstance(model_params, str):
            model_params_path = os.path.join(os.path.dirname(filepath), model_params)
            with open(model_params_path) as file:
                model_params = json.load(file)
        model = load_model(model_params)

        minibatch_size = read(experiment, "minibatch_size")
        save_plots = read(experiment, "save_plots")
        optimizer = load_optimizer(read(experiment, "optimizer"))
        loss = losses[read(experiment, "loss")]
        name = read(experiment, "name")
        input_width = read(experiment, "input_width")
        show_plots = read(experiment, "show_plots")

        mode = read(experiment, "mode")["name"]
        mode_params = read(experiment, "mode")["params"]
        dataset_path = read(experiment, "dataset")
        save_log = read(experiment, "save_log")
        delta = read(experiment, "delta")

        samples = ds_manager.load(dataset_path)
        samples = preprocess(samples, delta)

        experiment = Experiment(minibatch_size=minibatch_size,
                                samples=samples,
                                model_params=model,
                                loss_operator=loss,
                                optimizer=optimizer,
                                save_plots=save_plots,
                                name=name,
                                save_log=save_log,
                                input_width=input_width,
                                show_plots=show_plots)
        runners.append(ExperimentRunner(experiment, mode, mode_params))

    return runners


def load_optimizer(optimizer_params):
    return optimizers[optimizer_params["name"]](**optimizer_params["params"])


class DatasetsManager:
    def __init__(self):
        self.datasets = {}

    def load(self, path):
        if path not in self.datasets.keys():
            print('Loading dataset: {}'.format(path))
            print('...')
            self.datasets[path] = load_from_separate_files(path)
            print('Loaded')
        return self.datasets[path]


class ExperimentRunner:
    def __init__(self, experiment: Experiment, mode, mode_params):
        self.experiment: Experiment = experiment
        self.mode = mode
        self.mode_params = mode_params

    def run(self):
        if self.mode == 'run':
            self.experiment.run(**self.mode_params)
        elif self.mode == 'run_k_fold':
            self.experiment.run_k_fold(**self.mode_params)


def preprocess(samples, delta=None):
    prep_samples = copy.copy([sample[0] for sample in samples])
    prep_labels = copy.copy([sample[1] for sample in samples])
    if delta is not None:
        samples_delta = [librosa.feature.delta(sample[0], delta, axis=1) for sample in samples]
        prep_samples = np.array([np.concatenate((sample[0], sample_delta), axis=-1) for sample, sample_delta in zip(samples, samples_delta)])
    return [(sample, labels) for sample, labels in zip(prep_samples, prep_labels)]
