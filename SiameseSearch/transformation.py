from spect import Signal, Spectrogram
from scipy.ndimage.interpolation import shift
from scipy.misc import imresize
import numpy as np
import random
from copy import deepcopy


def ident(spect):
    return spect


class ShiftTransform:
    def __init__(self, shift_seq):
        self.shift_seq = shift_seq

    def transform(self, spect):
        a = shift(spect.D, shift=self.shift_seq)
        spect.D = a
        return spect


class StretchTransform:
    def __init__(self, factor):
        self.factor = factor

    def transform(self, spect):
        shape = spect.D.shape
        size = [int(shape[0] * self.factor[0]), int(shape[1] * self.factor[1])]
        scaled = imresize(spect.D, size=size, mode='F')[:shape[0], :shape[1]]
        new = np.zeros(shape)
        new[:size[0], :size[1]] = scaled
        spect.D = new
        return spect


class GaussianNoiseTransform:
    def __init__(self, mean, std):
        self.mean = mean
        self.std = std

    def transform(self, spect):
        noise = np.maximum(np.random.normal(self.mean, self.std, spect.D.shape), np.zeros(spect.D.shape))
        spect.D = spect.D + noise
        return spect


def test_transform(trans_func, signal, out_path='out.wav'):
    spect = Signal.stft(signal)
    spect.D = np.absolute(spect.D)
    trans_spect = trans_func(spect)
    spect.specshow()

    inv_signal = Spectrogram.inverse(trans_spect)
    Signal.save(inv_signal, out_path)
    return inv_signal


def ident_trans_test(path):
    signal = Signal.load(path)
    return test_transform(ident, signal, 'ident.wav')


def shift_trans_test(path, shift_seq):
    shift_t = ShiftTransform(shift_seq)
    signal = Signal.load(path)
    return test_transform(shift_t.transform, signal, 'shift.wav')


def stretch_trans_test(path, factor):
    stretch_t = StretchTransform(factor)
    signal = Signal.load(path)
    return test_transform(stretch_t.transform, signal, 'stretch.wav')


def noise_trans_test(path, mean, std):
    noise_t = GaussianNoiseTransform(mean, std)
    signal = Signal.load(path)
    return test_transform(noise_t.transform, signal, 'noise.wav')


def random_shift_trans(x_range, y_range):
    x = random.randint(x_range[0], x_range[1])
    y = random.randint(y_range[0], y_range[1])
    return ShiftTransform([x, y])


def random_stretch_trans(x_range, y_range):
    x = random.uniform(x_range[0], x_range[1])
    y = random.uniform(y_range[0], y_range[1])
    return StretchTransform([x, y])


def random_clip_spectrum(spectrum, output_length):
    if output_length is None:
        return spectrum
    input_width = np.shape(spectrum.D)[1]
    output_width = 1 + int((output_length/1000 * spectrum.sr - spectrum.n_fft)/spectrum.hop_length)
    if output_width > input_width:
        arr = np.zeros((np.shape(spectrum.D)[0], output_width))
        arr[:, 0:input_width] = spectrum.D
        spectrum.D = arr
    else:
        beg = random.randint(0, input_width - output_width)
        spectrum.D = spectrum.D[:, beg:output_width + beg]
    return spectrum


class TransformGenerator:
    def __init__(self, stretch_ranges, out_width=None, noise_params=(0, 0.00), multiplier=1):
        self.out_width = out_width
        self.stretch_ranges = stretch_ranges
        self.noise_params = noise_params
        self.multiplier = multiplier

    def random_trans(self, spectrum):
        transformed = []
        for _ in range(self.multiplier):
            stretch_trans = random_stretch_trans(*self.stretch_ranges)
            noise_trans = GaussianNoiseTransform(*self.noise_params)
            spect = deepcopy(spectrum)
            clipped = random_clip_spectrum(spect, self.out_width)
            noised = noise_trans.transform(clipped)
            transformed.append((stretch_trans.transform(noised)))
        return transformed
