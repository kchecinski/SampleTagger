import matplotlib.pyplot as plt


class Logger:
    def __init__(self):
        self.logs = {}
        self.info = {}

    def __getitem__(self, item):
        new = Logger()
        if hasattr(item, '__iter__'):
            for it in item:
                if it in self.logs.keys():
                    new.logs[it] = self.logs[it]
        elif item in self.logs.keys():
            new.logs[item] = self.logs[item]
        return new

    def add_value(self, name, value):
        if name in self.logs.keys():
            self.logs[name].append(value)
        else:
            self.logs[name] = [value]

    def add_info(self, key, value):
        self.info[key] = value

    def get_info(self, key):
        return self.info.get(key, None)

    def reset(self):
        self.logs = {}

    def get_values(self, name):
        return self.logs.get(name, [])

    def reduce(self, oper):
        new = Logger()
        for key in self.logs.keys():
            new.logs[key] = [oper(self.logs[key])]
        return new

    def add_logs(self, logger, prefix=""):
        for items in logger.logs.items():
            self.logs[prefix + items[0]] = self.logs.get(prefix + items[0], []) + items[1]

    def lines_count(self):
        return min([len(log) for log in self.logs.values()])

    def get_line(self, i, keys=None, add_index=True, names=False, separator='\t'):
        res = ""
        if i < 0:
            i = self.lines_count() + i
        if add_index:
            res += str(i) + separator
        if keys is None:
            keys = self.logs.keys()
        if names:
            res += separator.join(['{}: {}'.format(key, self.logs[key][i]) for key in keys])
        else:
            res += separator.join([str(self.logs[key][i]) for key in keys])
        return res

    def print_last_line(self, keys=None):
        if keys is None:
            keys = self.logs.keys()

        print("\t".join(["{}: {}".format(key, self.logs[key][-1]) for key in keys]))

    def generate_plot(self, plot_name, keys=None, show=False, save_path=None,
                      x_scale='linear', y_scale='linear'):
        if not show and save_path is None:
            return
        plt.figure(plot_name)
        lines = []
        for key in keys:
            line, = plt.plot(self.logs.get(key, []), label=key)
            lines.append(line)

        plt.legend(handles=lines)
        plt.xscale(x_scale)
        plt.yscale(y_scale)
        try:
            if save_path is not None:
                plt.savefig(save_path + plot_name)
            if show:
                plt.show()
        except ValueError:
            print('plot error')

    def header(self, keys=None, separator='\t', add_index=True):
        index = 'i' + separator if add_index else ''
        if keys is None:
            keys = self.logs.keys()
        return index + separator.join(keys) + '\n'

    def all_lines(self, keys=None, add_index=True, names=False, separator='\t'):
        if keys is None:
            keys = self.logs.keys()
        lines = [self.get_line(i,
                               add_index=add_index, names=names,
                               keys=keys, separator=separator) + '\n' for i in range(self.lines_count())]
        return lines

    def save_log(self, file_path, keys=None, header=True, add_index=True, names=False, separator='\t'):
        if keys is None:
            keys = self.logs.keys()

        with open(file_path, 'a') as f:
            if header:
                f.write(self.header(keys=keys, add_index=add_index))
            lines = self.all_lines(keys=keys, add_index=add_index, names=names, separator=separator)
            f.writelines(lines)

    def has_key(self, key):
        return key in self.logs.keys()

