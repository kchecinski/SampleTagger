import models.model_components as layers
from models.SiameseNetwork import SiameseNetwork
import tensorflow as tf
from scipy.signal import medfilt
import json


activations = {
    None: None,
    "relu": tf.nn.relu,
    "sigmoid": tf.nn.sigmoid
}


def load_model(model_params):
    convs_params = model_params["convs_params"]
    denses_params = model_params["denses_params"]
    twin_dense_params = model_params.get('twin_dense', None)
    model = Model(convs_params=convs_params, denses_params=denses_params, twin_dense=twin_dense_params)
    return model


class Model:
    def __init__(self,
                 convs_params,
                 twin_dense,
                 denses_params):
        self.convs_params = convs_params
        self.twin_dense = twin_dense
        self.denses_params = denses_params

    def build_model(self, input_shape):
        twin_layers = []
        for i, params in enumerate(self.convs_params):
            strides = params.get('strides', [1, 1])
            twin_layers.append(layers.ConvLayer(params['filters_count'], params['filters_size'], strides=strides,
                               padding=params.get('padding', 'same'), name='conv' + str(i)))
            twin_layers.append(layers.MaxPoolLayer([1, 2, 2, 1],
                                                   strides=[1, 2, 2, 1],
                                                   padding='VALID', name='maxpool' + str(i)))
            if params.get('dropout', None) is not None:
                twin_layers.append(layers.DropoutLayer(params['dropout']))

        twin_layers.append(layers.FlattenLayer())

        if self.twin_dense is not None:
            for i, params in enumerate(self.twin_dense):
                twin_layers.append(layers.DenseLayer(params['size'],
                                                     activations[params['activation']],
                                                     name='twindense' + str(i)))

        twin = layers.LayeredModel(twin_layers)

        distance_layers = list()
        # distance_layers.append(layers.DropoutLayer(0.3, 'dropout'))
        for i, params in enumerate(self.denses_params):
            if params.get('dropout', None) is not None:
                distance_layers.append(layers.DropoutLayer(params['dropout']))
            distance_layers.append(layers.DenseLayer(params['size'],
                                                     activation=activations[params['activation']],
                                                     name='dense' + str(i)))

        distance_layers.append(layers.DenseLayer(1, name='output'))

        distance = layers.LayeredModel(distance_layers)

        x1 = tf.placeholder(dtype='float', shape=input_shape, name='x1')
        x2 = tf.placeholder(dtype='float', shape=input_shape, name='x2')

        siamese = SiameseNetwork(twin, distance)
        siamese.build(x1, x2)

        return x1, x2, siamese

    def to_json(self, path):
        structure = {'convs_params': self.convs_params, 'denses_params': self.denses_params}
        with open(path, 'w') as file:
            json.dump(structure, file)
