import tensorflow as tf


class LayeredModel:

    def __init__(self, layers_builders):
        self.layers_builders = layers_builders
        self.layers = []

    def __call__(self, x):
        curr_out = x
        for builder in self.layers_builders:
            curr_out = builder(curr_out)
            self.layers.append(curr_out)
            print(self.layers[-1].shape)
        return curr_out


class ConvLayer:

    def __init__(self,
                 filters,
                 kernel_size,
                 strides=(1, 1),
                 padding='valid',
                 dilation_rate=(1, 1),
                 activation=tf.nn.relu,
                 use_bias=True,
                 name=None):
        self.filters = filters
        self.kernel_size = kernel_size
        self.strides = strides
        self.padding = padding
        self.dilation_rate = dilation_rate
        self.activation = activation
        self.use_bias = use_bias
        self.name = name

    def __call__(self, x):
        return tf.layers.conv2d(inputs=x,
                                filters=self.filters,
                                kernel_size=self.kernel_size,
                                strides=self.strides,
                                padding=self.padding,
                                dilation_rate=self.dilation_rate,
                                activation=self.activation,
                                use_bias=self.use_bias,
                                name=self.name)


class MaxPoolLayer:

    def __init__(self,
                 ksize,
                 strides,
                 padding,
                 name=None):
        self.ksize = ksize
        self.strides = strides
        self.padding = padding
        self.name = name

    def __call__(self, x):
        return tf.nn.max_pool(x,
                              ksize=self.ksize,
                              strides=self.strides,
                              padding=self.padding,
                              name=self.name)


class DenseLayer:

    def __init__(self,
                 units,
                 activation=None,
                 use_bias=True,
                 kernel_initializer=None,
                 bias_initializer=tf.zeros_initializer,
                 name=None):
        self.units = units
        self.activation = activation
        self.use_bias = use_bias
        self.kernel_initializer = kernel_initializer
        self.bias_initializer = bias_initializer
        self.name = name

    def __call__(self, x):
        return tf.layers.dense(x,
                               units=self.units,
                               activation=self.activation,
                               use_bias=self.use_bias,
                               kernel_initializer=self.kernel_initializer,
                               bias_initializer=self.bias_initializer,
                               name=self.name)


class FlattenLayer:

    def __init__(self,
                 name=None):
        self.name = name

    def __call__(self, x):
        return tf.layers.flatten(x, name=self.name)


class DropoutLayer:

    def __init__(self,
                 keep_prob,
                 name=None):
        self.keep_prob = keep_prob
        self.name = name

    def __call__(self, x):
        return tf.nn.dropout(x,
                             keep_prob=self.keep_prob,
                             name=self.name)
