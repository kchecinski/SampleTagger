from spect import Signal


class SampleInfo:
    def __init__(self, smpl_ids: list, smpl_path, smpl_begin: int, smpl_end: int):
        self.ids = smpl_ids
        self.begin = int(smpl_begin)
        self.end = int(smpl_end)
        self.path = smpl_path


def align(info: SampleInfo):
    if info.begin < 0:
        diff = -info.begin
        info.begin += diff
        info.end += diff


class ClipTask:
    def __init__(self, audio_container=None):
        self.audio_container = audio_container

    def clip(self, info: SampleInfo):
        align(info)
        if self.audio_container is None:
            signal = Signal.load(info.path)
        else:
            signal = self.audio_container.samples[info.path]
        clipped = Signal.clip(signal, info.begin, info.end, pad=True)
        ids = [int(ident) for ident in info.ids.split(',')]
        return clipped, ids


def read_samples_info(info_path):
    with open(info_path) as file:
        lines = file.readlines()
    return [SampleInfo(*sample_data.split('\t')[0:4]) for sample_data in lines]


class ExpandSampleTask:
    def __init__(self, expand_tuple, length):
        self.expand_tuple = expand_tuple
        self.length = length

    def expand(self, info):
        info.begin = info.begin + self.expand_tuple[0]
        info.end = info.end + self.expand_tuple[1]

        return info
