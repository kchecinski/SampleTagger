from spect import array2bitmap, Signal


class SavingManager:
    def __init__(self, file_name):
        self.file_name = file_name
        self.id = 0

    def save_bitmap(self, array):
        file_name = self.file_name + str(self.id)
        array2bitmap(array[0], file_name)
        self.id += 1

    def save_sound(self, signal):
        file_name = self.file_name + str(self.id)
        Signal.save(signal, file_name)
        self.id += 1
