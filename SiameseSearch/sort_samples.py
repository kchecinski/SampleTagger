import operator
import sys


def sort_samples(samples):
    return sorted(samples, key=lambda x: (x[1], int(x[2])))


def main(in_filename, out_filename):
    with open(in_filename) as file:
        rows = [row.split('\t') for row in file.readlines()]
    sorted_rows = ['\t'.join(row) for row in sort_samples(rows)]
    if out_filename is None:
        print(*sorted_rows)
    else:
        with open(out_filename, 'w') as file:
            file.writelines(sorted_rows)


if __name__ == "__main__":
    in_file = sys.argv[1]
    out_file = sys.argv[2] if len(sys.argv) > 2 else None
    main(in_file, out_file)

