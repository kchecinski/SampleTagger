from spect import Signal
import sys

if __name__ == '__main__':
    if len(sys.argv) < 5:
        print("Too few arguments. 4 arguments required: \n"
              " input path, output path, sample_begin [ms], sample duration[ms]")
    in_path = sys.argv[1]
    out_path = sys.argv[2]
    sample_begin = int(sys.argv[3])
    sample_duration = int(sys.argv[4])

    signal = Signal.load(in_path)
    clipped = Signal.clip(signal, sample_begin, sample_begin + sample_duration)
    Signal.save(clipped, out_path)


