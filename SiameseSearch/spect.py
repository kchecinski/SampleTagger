import numpy as np
from matplotlib import pyplot as plt
import librosa
import librosa.display
import copy
import scipy.stats as stats
from PIL import Image


class Signal:

    def __init__(self, y, sr):
        self.y, self.sr = y, sr

    def length(self):
        return int(self.y.shape[0] * 1000 / self.sr)

    @classmethod
    def load(cls, path, sr=None):
        return cls(*librosa.load(path, sr=sr))

    @staticmethod
    def save(signal, path):
        librosa.output.write_wav(path, signal.y, signal.sr)

    @staticmethod
    def clip(signal, begin, end, pad=False):
        out = copy.copy(signal)
        out.y = signal.y[int(np.round(begin*signal.sr/1000)):int(np.round(end*signal.sr/1000))]
        if pad:
            out.y = np.concatenate((out.y, np.zeros(int(np.round((end-begin)*signal.sr/1000)) - len(out.y))))
        return out

    @staticmethod
    def stft(signal, n_fft=2048, hop_length=None, win_length=None, window='hann', center=True):
        return Spectrogram(signal, n_fft, hop_length, win_length, window, center)

    @staticmethod
    def melspect(signal, n_fft=2048, n_mels=128, fmin=0.0, fmax=None, power=1.0):
        return Melspectrum(signal, n_fft=n_fft, n_mels=n_mels, fmin=fmin, fmax=fmax, power=power)

    @staticmethod
    def mfcc(signal, n_mfcc=20, display=False):
        s = librosa.feature.mfcc(signal.y, signal.sr, n_mfcc=n_mfcc)
        if display:
            librosa.display.specshow(s, x_axis='time')
            plt.colorbar()
        return s


class Spectrogram:

    def __init__(self, signal=None, n_fft=2048, hop_length=None, win_length=None, window='hann', center=True,
                 d=None, sr=None):
        self.win_length = n_fft if win_length is None else win_length
        self.hop_length = int(self.win_length/4) if hop_length is None else hop_length
        self.n_fft = n_fft
        self.window = window
        self.center = center
        if signal is not None:
            self.D = librosa.stft(signal.y, n_fft=self.n_fft, hop_length=self.hop_length, win_length=self.win_length,
                                  window=self.window)
            self.sr = signal.sr
        else:
            self.D = d
            self.sr = sr

    def specshow(self, x_axis='time', y_axis='hz', bar=True):
        librosa.display.specshow(librosa.amplitude_to_db(self.D, ref=np.max), sr=self.sr,
                                 hop_length=self.hop_length, x_axis=x_axis, y_axis=y_axis)
        plt.title('Power spectrogram')
        if bar:
            plt.colorbar(format='%+2.0f dB')
        plt.tight_layout()

    @staticmethod
    def inverse(spec):
        y = librosa.istft(spec.D, spec.hop_length, spec.win_length, spec.window)
        return Signal(y, spec.sr)

    @staticmethod
    def trim(spec, new_sr):
        levels = np.shape(spec.D)[0]
        new_levels = int(levels * new_sr / spec.sr)
        print(new_levels)
        s = copy.deepcopy(spec)
        s.D[new_levels:, ] = np.zeros(np.shape(s.D[new_levels:, ]))
        return s

    @staticmethod
    def avg_band(spec, ratio, first_band=1, mean='lin', amplif=1, clip_band=None):
        s = copy.deepcopy(spec)
        s.D = np.zeros(s.D.shape)
        curr_f = 0
        i = 0
        curr_width = first_band
        s_levels = int(spec.D.shape[0] if clip_band is None else spec.D.shape[0] * clip_band * 2 / spec.sr) + 1
        amp = 1
        if ratio == 1:
            z_levels = s_levels/first_band
        else:
            z_levels = np.log(1-s_levels*(1-ratio)/first_band)/np.log(ratio) + 1
        z = np.ndarray(shape=(int(z_levels), spec.D.shape[1]))
        if mean != 'lin' and mean != 'geom':
            raise NameError('Unknown mean type: ' + mean)
        mean_func = np.mean if mean == 'lin' else stats.mstats.gmean
        if first_band == 1 and ratio == 1:
            z = spec.D[:s_levels, :]
            return s, z
        while curr_f < s_levels:
            curr_width_r = int(round(curr_width))
            z[i, :] = mean_func(spec.D[curr_f:curr_f + curr_width_r, :], axis=0)
            curr_f += curr_width_r
            curr_width = curr_width*ratio
            i = i + 1
            if amplif is not None:
                amp *= amplif
        return s, z

    @staticmethod
    def melspectrum(spec, n_fft=2048, n_mels=128):
        spec.D = librosa.feature.melspectrogram(spec.D, n_fft=n_fft, n_mels=n_mels)

    @staticmethod
    def to_absolute(spec):
        spec.D = np.absolute(spec.D)
        return spec


class Melspectrum:
    def __init__(self, signal, n_fft=2048, n_mels=128, fmin=0.0, fmax=None, hop_length=None, power=1.0):
        self.n_fft = n_fft
        self.n_mels = n_mels
        self.fmin = fmin
        self.fmax = fmax
        self.sr = signal.sr
        self.hop_length = n_fft / 4 if hop_length is None else hop_length
        self.D = librosa.feature.melspectrogram(signal.y, signal.sr,
                                                n_fft=n_fft, n_mels=n_mels,
                                                fmin=fmin, fmax=fmax, hop_length=hop_length, power=power)


def process(path_in, ratio, first_band=1, mean='geom', path_wav=None, path_img=None, amplif=1):
    signal = Signal.load(path_in)
    spectrum = Signal.stft(signal)
    spect_avg, comp = Spectrogram.avg_band(spectrum, ratio, first_band, mean, amplif)
    print('ratio: ', ratio, '\tfirst: ', first_band)
    print(comp.shape)
    signal_avg = Spectrogram.inverse(spect_avg)
    if path_wav is None:
        path_wav = 'results/' + str(ratio) + '-' + str(first_band) + '-' + str(amplif) + '-' + mean + '.wav'
    if path_img is None:
        path_img = 'results/' + str(ratio) + '-' + str(first_band) + '-' + str(amplif) + '-' + mean + '.bmp'
    Signal.save(signal_avg, path_wav)
    array2bitmap(comp, path_img)


def get_samples(path, samplerate = None):
    signal = Signal.load(path, samplerate)
    return signal.y


def normalize(data, a=0, b=1):
    d_abs = np.absolute(data)
    d_min = np.min(d_abs)
    d_max = np.max(d_abs)
    d_scope = d_max - d_min
    if d_scope == 0:
        return 0
    else:
        return (d_abs - d_min)*(b - a)/d_scope + a


def array2bitmap(data, path):
    bitmap_data = normalize(data, 0, 255)
    img = Image.fromarray(np.uint8(bitmap_data), 'L')
    img.save(path + '.bmp')
