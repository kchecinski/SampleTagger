import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from search_engine.search_engine import load_model_info
from data_serialization import load_from_separate_files
from Dataset import GenerativeDataset
import numpy as np
import itertools
import tensorflow as tf


def prediction_op(output):
    return tf.cast(tf.greater(output, 0.5), dtype=tf.float32)


def prediction_error_op(prediction, labels):
    return tf.reduce_mean(tf.cast(tf.equal(prediction, labels), dtype=tf.float32))


def prepare_model(path, input_shape):
    model, model_info = load_model_info(path)
    x1, x2, y = model.build_model(input_shape=input_shape)
    labels = tf.placeholder(tf.float32, y.out.shape, name='labels')
    pred_op = prediction_op(y.out)
    pred_err_opt = prediction_error_op(pred_op, labels)
    return (x1, x2, labels, pred_err_opt), model_info


def prepare_dataset(path, output_width):
    samples = load_from_separate_files(path)
    return GenerativeDataset(output_width, samples)


def eval_confusion_matrix(dataset, model, sess):
    x1, x2, labels, pred_op = model
    mb_size = x1.shape[0]
    # labels = tf.reshape(labels, [-1])
    # pred_op = tf.reshape(pred_op, [-1])
    labels_list = []
    pred_list = []
    for i in range(100):
        minibatch = dataset.get_pairs_minibatch(mb_size)
        minibatch_dict = {x1: minibatch[0], x2: minibatch[1], labels: minibatch[2]}
        pred_list.append(sess.run(pred_op, feed_dict=minibatch_dict))
        labels_list.append(minibatch[2])
    labels_list = np.concatenate(labels_list, axis=0)
    pred_list = np.concatenate(pred_list, axis=0)
    confusion = confusion_matrix(labels_list, pred_list)
    plt.matshow(confusion)
    plt.show()


def eval_same_pair_accuracy(dataset, model, sess):
    keys = dataset.keys()
    res = dict()
    for key in keys:
        acc = eval_all_pairs(key, key, dataset, model, sess)

        res[key] = acc
        # print("{}\t{}%".format(key, acc * 100))

    return res


def eval_all_pairs(key1, key2, dataset, model, sess):
    x1, x2, labels, pred_op = model
    mb_size = x1.shape[0]

    key1_c = len(dataset.samples[key1])
    key2_c = len(dataset.samples[key2])

    pairs = [pair for pair in itertools.product(range(key1_c), range(key2_c))]

    print('keys: {}, {}\tpairs: {}'.format(key1, key2, len(pairs)))

    pairs_chunks = [pairs[i: i + mb_size] for i in range(0, len(pairs), mb_size)]

    preds = list()

    for pairs_chunk in pairs_chunks:
        x1s = np.stack([dataset.samples[key1][a][0][:, :220] for a, _ in pairs_chunk])
        x2s = np.stack([dataset.samples[key2][b][0][:, :220] for _, b in pairs_chunk])
        ys = np.stack([np.array([0 if key1 == key2 else 1])] * mb_size)

        minibatch_dict = {x1: x1s, x2: x2s, labels: ys}
        pred_list = sess.run(pred_op, feed_dict=minibatch_dict)
        print(key1, key2, pred_list)
        preds.append(pred_list)

    acc = np.mean(preds)

    return acc


def eval_different_pair_accuracy(dataset, model, sess):
    keys = dataset.keys()
    combinations = itertools.combinations_with_replacement(keys, 2)
    indexes = itertools.combinations_with_replacement(range(len(keys)), 2)
    mat = np.zeros([len(keys), len(keys)])
    for comb, index in zip(combinations, indexes):
        print(comb[0], comb[1])
        acc = eval_all_pairs(comb[0], comb[1], dataset, model, sess)
        mat[index[0], index[1]] = acc
        mat[index[1], index[0]] = acc
    return mat


def bar_plot_labels(items, keywords, label):
    fig, ax = plt.subplots()
    row_len = len(items)
    index = np.arange(row_len)
    ax.bar(index, [item for item in items], 0.5)
    ax.set_ylabel(label)
    ax.set_xticks(index)
    ax.set_xticklabels(keywords, rotation='vertical')
    plt.show()


def mat_plot_labels(mat, keywords):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(mat)
    fig.colorbar(cax, orientation='horizontal')

    ax.set_xticks(np.arange(len(keywords)))
    ax.set_xticklabels(keywords, rotation='vertical')
    ax.set_yticks(np.arange(len(keywords)))
    ax.set_yticklabels(keywords)
