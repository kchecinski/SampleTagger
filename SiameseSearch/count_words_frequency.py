import re
import sys


def count_frequency(text):
    frequency = {}
    lower_text = text.lower()
    match_pattern = re.findall(r'\b[a-zżźćńółęąś]{3,20}\b', lower_text)

    print(len(match_pattern))

    for word in match_pattern:
        count = frequency.get(word, 0)
        frequency[word] = count + 1

    freq_list = frequency.items()
    return freq_list


def main():
    path = sys.argv[1]
    if len(sys.argv) >= 3:
        sort_mode = int(sys.argv[2])
    else:
        sort_mode = 0
    text = open(path, 'r').read()

    result = count_frequency(text)
    result = sorted(result, key=lambda x: x[sort_mode])

    for i, word_freq in enumerate(result):
        #print("{}: {} - {}".format(i, word_freq[0], word_freq[1]))
        print("{} {}".format(word_freq[0],word_freq[1]))


if __name__ == "__main__":
    main()
