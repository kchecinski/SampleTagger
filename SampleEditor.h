#ifndef SAMPLEEDITOR_H
#define SAMPLEEDITOR_H

#include <QDialog>
#include <QModelIndex>
#include "Sample.h"
#include "KeywordsBase.h"

namespace Ui {
class SampleEditor;
}

class SampleEditor : public QDialog
{
    Q_OBJECT

public:
    explicit SampleEditor(QWidget *parent = nullptr);
    ~SampleEditor();

    void setSample(const Sample& sample, QModelIndex index);
    void setKeywordsBase(KeywordsBase *base);

    QSet<int> getLabels() const;
    int getStart() const;
    int getEnd() const;
    QString getAdditionalInfo() const;

    QModelIndex getIndex() const;

signals:
    void okButtonClicked();
    void deleteButtonClicked();

private slots:
    void addLabel(int id);
    void synchronizeBegin(int i);
    void synchronizeEnd(int i);

private:
    Ui::SampleEditor *ui;

    int frame_length_;
    QModelIndex index_;
    QString labels_str_;
};

#endif // SAMPLEEDITOR_H
