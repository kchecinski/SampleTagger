
#ifndef SNATCH_H
#define SNATCH_H

#include <QUrl>
#include <QSet>

class Sample
{
public:
    Sample(QString path, int begin, int end, const QString& info = QString());
    Sample(QUrl url, int begin, int end, const QString& info = QString());

    int getBegin() const;
    int getEnd() const;
    const QString getPath() const;
    QSet<int> getIds() const;
    QString getAdditionalInfo() const;

    void setBegin(int begin);
    void setEnd(int end);
    void setIds(const QSet<int>& ids);
    void setAdditionalInfo(const QString& info);

    void addId(int id);

    QString toString(const QString& separator, const QString &ids_separator) const;

    static Sample fromString(const QString& data, const QString& separator, const QString &ids_separator);

private:
    QSet<int> ids_;
    QString path_;
    int begin_;
    int end_;
    QString add_info_;

    static QString urlToPath(QUrl url);
};

#endif // SNATCH_H
